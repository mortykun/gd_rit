extends Node2D

class_name GameZone

export(String) var zone_name
export(String) var zone_description
export(String) var starting_room

onready var curr_room = $curr_room
onready var room_list = $room
onready var ui_area = $CanvasLayerUI/UIArea


func _ready():
    if not curr_room:
        return

    room_list.visible = false

    XGame.player.rebuild()

    for cr in room_list.get_children():
        cr.connect("goto_room", self, "_goto_room")

    if starting_room:
        _goto_room(starting_room, true)


func _goto_room(room_name: String, skip_checks: bool = false) -> void:

    if room_name == '__quit__':
        SceneChanger.main_menu()
        return

    var rnode: GameRoom = room_list.find_node(room_name, false, false)

    if not rnode:
        Global.show_error('Room [%s] not found in zone.' % room_name)
        return

    var cnode: GameRoom = null if curr_room.get_child_count() == 0 else curr_room.get_child(0)

    if cnode:
        if not skip_checks:
            if not cnode.can_exit_room(room_name):
                return

    if not skip_checks:
        if not rnode.can_enter_room("" if not cnode else cnode.room_name):
            return

    if cnode:
        cnode.on_exit_room()
        curr_room.remove_child(cnode)
        room_list.add_child(cnode)

    room_list.remove_child(rnode)
    curr_room.add_child(rnode)
    rnode.on_enter_room()
