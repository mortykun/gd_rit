tool
extends Node2D

enum RoomStatus { ACTIVE, DISABLED, HIDDEN }
enum RoomLabelShow { ALWAYS, ON_HOVER, NEVER }
enum RoomLabelPos { TOP, BOTTOM, LEFT, RIGHT }

export(String) var game_room_name
export(String) var exit_node_name
export(RoomStatus) var room_status setget _room_status_set
export(RoomLabelShow) var show_label setget _room_show_label_set
export(Texture) var exit_image
export(RoomLabelPos) var label_pos setget _room_label_pos_set
export(Resource) var mouse_hover_shader

signal exit_room

onready var exit_lbl := $Area2D/exit_lbl

func _ready():
    exit_lbl.text = game_room_name

    if exit_image:
        $Area2D/img_static.texture = exit_image

    $Area2D/col_shape.shape.extents = Vector2($Area2D/img_static.texture.get_size() * 0.5)

    exit_lbl.rect_position = Vector2.ZERO
    toggle_label_show(false)


func toggle_label_show(mouse_hover: bool) -> void:
    if not exit_lbl:
        return

    exit_lbl.hide()

    if show_label == RoomLabelShow.NEVER:
        return

    if show_label == RoomLabelShow.ALWAYS:
        exit_lbl.show()

    elif show_label == RoomLabelShow.ON_HOVER:
        if not mouse_hover:
            return

        exit_lbl.show()

    if exit_lbl.rect_position.x == 0:
        if label_pos == RoomLabelPos.LEFT:
            exit_lbl.rect_position.x = 0 - (($Area2D/img_static.texture.get_size().x * 0.5) + exit_lbl.rect_size.x + 5)
        elif label_pos == RoomLabelPos.RIGHT:
            exit_lbl.rect_position.x = ($Area2D/img_static.texture.get_size().x * 0.5) + 5
        else:
            exit_lbl.rect_position.x = 0 - (exit_lbl.rect_size.x * 0.5)

    if exit_lbl.rect_position.y == 0:
        if label_pos == RoomLabelPos.TOP:
            exit_lbl.rect_position.y = 0 - (exit_lbl.rect_size.y + 5 + ($Area2D/img_static.texture.get_size().y * 0.5))
        elif label_pos == RoomLabelPos.BOTTOM:
            exit_lbl.rect_position.y = 0 + ($Area2D/img_static.texture.get_size().y * 0.5)
        else:
            exit_lbl.rect_position.y = ($Area2D/img_static.texture.get_size().y * 0.5) - (exit_lbl.rect_size.y + 5)


func _room_status_set(new_value: int) -> void:
    if new_value == RoomStatus.ACTIVE:
        visible = true

    elif new_value == RoomStatus.DISABLED:
        visible = true

    elif new_value == RoomStatus.HIDDEN:
        visible = false

    else:
        return

    room_status = new_value


func _room_show_label_set(new_value: int) -> void:
    show_label = new_value
    toggle_label_show(false)

func _room_label_pos_set(new_value: int) -> void:
    label_pos = new_value
    if exit_lbl:
        exit_lbl.rect_position = Vector2.ZERO
    toggle_label_show(false)


func _exit_room_denied() -> String:
    return ""


func _on_Area2D_mouse_entered():
    toggle_label_show(true)

    if room_status == RoomStatus.HIDDEN:
        return

    if mouse_hover_shader:
        $Area2D/img_static.material = mouse_hover_shader
        $Area2D/img_animated.material = mouse_hover_shader


func _on_Area2D_mouse_exited():
    $Area2D/img_static.material = null
    $Area2D/img_animated.material = null

    toggle_label_show(false)

    if room_status == RoomStatus.HIDDEN:
        return



func _on_Area2D_input_event(_viewport, _event, _shape_idx):
    if _event.is_echo():
        return

    if _event is InputEventMouseButton:
        if _event.button_index == 1:
            if _event.is_pressed():
                print("exit pressed ", game_room_name)
                emit_signal("exit_room", exit_node_name)
