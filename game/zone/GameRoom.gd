extends Node2D

class_name GameRoom
# A GameRoom, is a scene that represents a single room, or place in the game world. A room has
#  exits, npcs, and items to interact with. A collection of rooms make up a single zone.

export(String) var room_name
export(String, MULTILINE) var room_description

signal goto_room


func _ready():
    for ext in $exit.get_children():
        ext.connect("exit_room", self, "_exit_room")


func on_enter_room() -> void:
    # The zone will call this method each time a player enters a room
    _init_bg()
    _init_exit()
    _init_npc()
    _init_item()


func on_exit_room() -> void:
    # The zone will call this method each time a player leaves a room
    pass


func can_enter_room(_from_room_name: String) -> bool:
    # Virtual method to check if the player can enter the room
    return true


func can_exit_room(_exit_node_name: String) -> bool:
    # Virtual method to check if the player can leave the room
    return true


func _exit_room(_exit_node_name: String) -> void:
    if not self.can_exit_room(_exit_node_name):
        return

    emit_signal("goto_room", _exit_node_name)


func _exit_denied(_next_room: String) -> void:
    pass


func _init_bg() -> void:
    # Virtual method to manage the background on startup
    pass


func _init_exit() -> void:
    # Virtual method to manage exits on startup
    pass


func _init_npc() -> void:
    # Virtual method to manage npcs on startup
    pass


func _init_item() -> void:
    # Virtual method to manage items on startup
    pass
