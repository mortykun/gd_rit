extends Resource

class_name GameItemClass

export(String) var id
export(String) var ic_name
export(String) var mod_item_group
export(PoolStringArray) var tags
