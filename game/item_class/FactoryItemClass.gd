extends Node


var _iclass: Dictionary = {}


func _ready():
    if Global.load_recursive_resources(_iclass, 'res://game/item_class', ['tres']) != OK:
        Global.show_error('Error loading item classes!')



func get_item_class(id: String) -> GameItemClass:
    var res: GameItemClass = _iclass.get(id)

    if not res:
        Global.show_error('GameItemClass not found [%s]' % id)

    return res
