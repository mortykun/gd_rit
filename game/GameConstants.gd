class_name GameConstants

enum DamageSource { ATTACK, SPELL }
enum DamageType { STRIKE, PIERCE, CRUSH, FIRE, COLD, LIGHTNING }

enum ItemSlot {
    HELM, BOOTS, GLOVES, CHEST, BELT,
    AMULET, LEFT_FINGER, RIGHT_FINGER,
    ON_HAND, OFF_HAND
}

enum ItemRarity { NORMAL, MAGIC, RARE, UNIQUE }

const PROP_ITEM_ORDER = [ 'dmg_att', 'dmg_spl', 'crit_ch', 'armor_att', 'armor_spl', 'aegis', 'dodge', 'str', 'dex', 'int']

const GAME_ITEM_MAX_MAGIC_AFFIX = 1
const GAME_ITEM_MAX_RARE_AFFIX = 3

const ITEM_WIDGET_COLOR_OK = Color(0.0, 1.0, 0.0, 0.3)
const ITEM_WIDGET_COLOR_INVALID = Color(1.0, 0.0, 0.0, 0.3)
const ITEM_WIDGET_COLOR_HIGHLIGHT = Color(0.0, 0.0, 1.0, 0.3)

const ITEM_TYPE_QUEST = "quest"
const ITEM_TYPE_CURRENCY = "currency"
const ITEM_TYPE_POTION = "potion"
const ITEM_TYPE_WEAPON = "weapon"
const ITEM_TYPE_SHIELD = "shield"
const ITEM_TYPE_QUIVER = "quiver"
const ITEM_TYPE_HELM = "helm"
const ITEM_TYPE_BOOTS = "boots"
const ITEM_TYPE_GLOVES = "gloves"
const ITEM_TYPE_CHEST = "chest"
const ITEM_TYPE_BELT = "belt"
const ITEM_TYPE_AMULET = "amulet"
const ITEM_TYPE_RING = "ring"


const ITEM_TYPES = {
    ITEM_TYPE_QUEST: {
        "drop_chance": 0
    },
    ITEM_TYPE_CURRENCY: {
        "drop_chance": 0,
        "sub_type": {
            "gold": null,
            "crystals": null, # { "drop_chance": 20 },
        },
    },
    ITEM_TYPE_WEAPON: {
        "drop_chance": 100,
        "sub_type": {
            "sword": { "drop_chance": 80 },
#            "rapier": { "drop_chance": 0 },
#            "axe":  { "drop_chance": 0 },
#            "mace":  { "drop_chance": 0 },
#            "spear":  { "drop_chance": 0 },
#            "bow":  { "drop_chance": 0 },
#            "dagger":  { "drop_chance": 0 },
#            "sceptre":  { "drop_chance": 0 },
#            "rod":  { "drop_chance": 0 },
#            "wand":  { "drop_chance": 0 },
#            "staff": { "drop_chance": 0 },
#            "orb": { "drop_chance": 0 },
        },
    },
    ITEM_TYPE_QUIVER: {
        "drop_chance": 0,
        "slot": ItemSlot.OFF_HAND,
        "slot_pair": [ "bow" ],
        "sub_type": {
            "leather": null,
            "battle": null,
            "side": null,
        },
    },
    ITEM_TYPE_SHIELD: {
        "drop_chance": 0,
        "slot": ItemSlot.OFF_HAND,
        "slot_pair": [ "sword", "rapier", "axe", "mace", "dagger", "sceptre", "rod", "wand", "orb" ],
        "sub_type": {
            "tower": null,
            "buckler": null,
            "steel": null,
            "round": null,
            "kite": null,
            "sword_breaker": null,
        },
    },
    ITEM_TYPE_RING: {
        "drop_chance": 20,
        "sub_type": {
            "ring_emerald": { "drop_chance": 100 },
            "ring_ruby": { "drop_chance": 100 },
            "ring_saphire": { "drop_chance": 100 },
        },
   },
}
