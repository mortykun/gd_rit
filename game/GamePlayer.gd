extends GameEntity

class_name GamePlayer

var combat := {}
var metrics := {}
var zone := {}
var stash := {}
var known_skills := {}
var known_spells := {}
var quests := {}
#var clothes := {}


func _init():
    pass


func is_player() -> bool:
    return true


func get_starting_stats() -> Dictionary:
    # Get the starting stats for this entity.
    return {
        'str': 5,
        'dex': 5,
        'int': 5,
    }


func load_all() -> bool:
    load_bio()
    stats = load_stats()
    # levels = load_levels()
    # items = load_items()
    quests = load_quests()

    return true


func save_all() -> int:
    save_bio()
    save_stats()
    # save_levels()
    # save_items()
    save_quests()

    return OK


func load_stats() -> Dictionary:
    var res := SaveGame.load_file(XGame.save_slot, 'stats')

    if not res:
        res = {}

    return res


func save_stats() -> int:
    var tr_stats = {}

    for stat in FactoryStat.get_stat_group(FactoryStat.Groups.TIER_TRAC):
        tr_stats[stat.stat_id] = stat

    return SaveGame.save_file(XGame.save_slot, 'stats', tr_stats)


func load_quests() ->  Dictionary:
    return SaveGame.load_file(XGame.save_slot, 'quests')


func save_quests() -> int:
    return SaveGame.save_file(XGame.save_slot, 'quests', quests)


func load_bio() -> void:
    var bio := SaveGame.load_file(XGame.save_slot, 'bio')

    if bio:
        name = bio['name']
        age = bio['age']
    else:
        name = ''
        age = 21


func save_bio() -> int:
    var data = {'name': name, 'gender': gender, 'age': age }

    return SaveGame.save_file(XGame.save_slot, 'bio', data)


func clear_statsh():
    stash.clear()
