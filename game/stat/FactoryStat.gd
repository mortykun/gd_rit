extends Node

const ABILITY_STATS = ["str", "dex", "int"]

enum Groups {TIER_TRAC, TIER_1, TIER_2, TIER_3, ABILITIES, STATS, DAMAGE }

var _stats: Dictionary = {}
var _groups: Dictionary = {}


func _ready():
    if Global.load_recursive_resources(_stats, 'res://game/stat', ['tres']) != OK:
        Global.show_error('Error loading body parts!')

    _init_groups()


func get_stat(id: String) -> GameStat:
    var res: GameStat = _stats.get(id)

    if not res:
        Global.show_error('GameStat not found [%s]' % id)

    return res


func get_stat_group(grp_id: int) -> Array:
    return _groups[grp_id]


func get_stat_ids_by_type(st: int) -> PoolStringArray:
    var res := PoolStringArray()

    for gs in _stats.values():
        if gs.stat_type == st:
            res.append(gs.id)

    return res


func _init_groups() -> void:
    # Builds the stat groups
    var tier1 = []
    var tier2 = []
    var tier3 = []
    var tiert = []
    var abils = []
    var stats = []
    var dmg = []

    var gs: GameStat

    for stat in _stats.values():
        gs = stat

        if gs.tier == GameStat.StatTier.ONE:
            tier1.append(stat)
        elif gs.tier == GameStat.StatTier.TWO:
            tier2.append(stat)
        elif gs.tier == GameStat.StatTier.THREE:
            tier3.append(stat)
        elif gs.tier == GameStat.StatTier.TRACKER:
            tiert.append(stat)

        if gs.stat_type == GameStat.StatType.STAT:
            if gs.tier != GameStat.StatTier.TRACKER:
                stats.append(stat)
        elif gs.stat_type == GameStat.StatType.ABILITY:
            abils.append(stat)
        elif gs.stat_type == GameStat.StatType.DAMAGE:
            dmg.append(stat)

    _groups[Groups.TIER_1] = tier1
    _groups[Groups.TIER_2] = tier2
    _groups[Groups.TIER_3] = tier3
    _groups[Groups.TIER_TRAC] = tiert
    _groups[Groups.ABILITIES] = abils
    _groups[Groups.STATS] = stats
    _groups[Groups.DAMAGE] = dmg
