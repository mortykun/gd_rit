# Holds the current calc value of a stat

class_name InstanceGameStat

const STAT_GRP_BASE = 'base'
const STAT_GRP_AFFIX = 'affx'
const STAT_GRP_LEVEL = 'level'
const STAT_GRP_GEAR = 'gear'
const STAT_GRP_TEMP = 'temp'

const STAT_GRPS = [ STAT_GRP_BASE, STAT_GRP_AFFIX, STAT_GRP_LEVEL, STAT_GRP_GEAR, STAT_GRP_TEMP ]

var stat_id: String  # The stat identifier
var _data: Dictionary = {}
var _value: Vector2   # The current value


func initialize(init_stat_id: String) -> void:
    stat_id = init_stat_id

    var gs: GameStat= FactoryStat.get_stat(stat_id)

    if gs.tier != GameStat.StatTier.TRACKER:
        _data[STAT_GRP_BASE] = []
        _data[STAT_GRP_LEVEL] = []
        _data[STAT_GRP_GEAR] = []
#        _data[STAT_GRP_TEMP] = []
#        _data['added'] = 0.0
#        _data['multi'] = 0.0


func get_stat() -> GameStat:
    return FactoryStat.get_stat(stat_id)


func clear() -> void:
    for grp in STAT_GRPS:
        mods_clear(grp)

    _value.x = 0
    _value.y = 0


func mod_add(stat_grp: String, gmv: InstanceGameMod) -> void:
    if gmv:
        if not _data.has(stat_grp):
            _data[stat_grp] = []

        _data[stat_grp].append(gmv)


func stat_add(igs: InstanceGameStat) -> void:
    for grp in STAT_GRPS:
        if not igs.data.has(grp):
            continue

        for mod in igs.data[grp]:
            mod_add(grp, mod)


func has_group_items(stat_grp: String) -> bool:
    if _data.get(stat_grp):
        return true

    return false


func mods_clear(stat_grp: String) -> void:
    if _data.has(stat_grp):
        _data[stat_grp].clear()


func get_value() -> float:
    return _value.x


func get_value_from() -> float:
    return _value.x


func get_value_to() -> float:
    return _value.y


func set_value(value: float) -> void:
    if _data:
        return

    _value.x = value
    _value.y = 0


func calc(ent_stats = null) -> void:
    var gs: GameStat = FactoryStat.get_stat(stat_id)

    if gs.tier == GameStat.StatTier.TRACKER and ent_stats:
        if gs.limit_id:
            var limit: InstanceGameStat = ent_stats.get(gs.limit_id)

            if limit and _value.x > limit.get_value():
                _value.x = limit.get_value()

        return

    _sum_mods()

    if gs.auto_calcs and ent_stats:
        _data['added'].x += _auto_calcs(gs, ent_stats)

    var tmp: Vector2 = _data['added']

    if _data['perc'].x != 0.0:
        tmp.x += _data['added'].x * _data['perc'].x * 0.01
        tmp.y += _data['added'].y * _data['perc'].y * 0.01


    if gs.stat_type == GameStat.StatType.DAMAGE:
        tmp.x = round(tmp.x)
        tmp.y = round(tmp.y)
    else:
        tmp.x = clamp(tmp.x, gs.min_value, gs.max_value)
        tmp.y = clamp(tmp.y, gs.min_value, gs.max_value)

    _value = tmp


func _sum_mods() -> void:
    var xadd = Vector2()
    var xperc = Vector2()
    var igm: InstanceGameMod

    for grp in STAT_GRPS:
        if not _data.has(grp):
            continue

        for mod in _data[grp]:
            igm = mod

            if igm.game_mod.calc == GameMod.ModCalc.ADDED:
                xadd += igm.value
            elif mod.game_mod.calc == GameMod.ModCalc.PERC:
                xperc += igm.value

    _data['added'] = xadd
    _data['perc'] = xperc


func _auto_calcs(gs: GameStat, ent_stats) -> float:
    var split: Array
    var oper: FuncRef
    var val1: float
    var val2: float
    var res: float = 0.0

    for ac in gs.auto_calcs:
        val1 = 0.0
        val2 = 0.0

        if ac.find('+') != -1:
            split = ac.split('+', 2)
            oper = funcref(Global, "op_add")
        elif ac.find('*') != -1:
            split = ac.split('*', 2)
            oper = funcref(Global, "op_mul")
        elif ac.find('-') != -1:
            split = ac.split('-', 2)
            oper = funcref(Global, "op_sub")
        elif ac.find('/') != -1:
            split = ac.split('/', 2)
            oper = funcref(Global, "op_div")
        else:
            split = ac.split('+', 2)
            oper = funcref(Global, "op_add")

        if not split:
            continue

        if split.size() == 1:
            if split[0].is_valid_float():
                val2 = float(split[0])
            else:
                val2 = ent_stats[split[0]].get_value()
        elif split.size() == 2:
            if split[0].is_valid_float():
                val1 = float(split[0])
            else:
                val1 = ent_stats[split[0]].get_value()

            if split[1].is_valid_float():
                val2 = float(split[1])
            else:
                val2 = ent_stats[split[1]].get_value()

        res += oper.call_func(floor(val1), floor(val2))

    return res
