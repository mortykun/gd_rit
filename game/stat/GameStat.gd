extends Resource

class_name GameStat

enum StatType { STAT, ABILITY, TALENT, ITEM, DAMAGE, DEFENSE, CRIT }
enum StatTier { TRACKER, ONE, TWO, THREE }

export(String) var id
export(String) var stat_name
export(String) var long_name
export(String, MULTILINE) var description
export(StatType) var stat_type
export(StatTier) var tier = StatTier.ONE
export(PoolStringArray) var tags = PoolStringArray()
export(PoolStringArray) var auto_calcs = PoolStringArray()
export(String) var limit_id
