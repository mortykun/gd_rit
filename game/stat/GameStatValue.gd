extends GameStat

class_name GameStatValue

export(float) var min_value = 0.0
export(float) var max_value = 999999.0
export(bool) var is_perc
export(int) var max_decimals = 0
