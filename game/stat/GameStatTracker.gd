# Holds the current value of a stat

class_name GameStatTracker

var stat_id: String  # The stat identifier
var _value: float   # The current tracked value


func initialize(init_stat_id: String, value: float = 0.0) -> void:
    stat_id = init_stat_id
    _value = value


func clear() -> void:
    _value = 0.0


func get_value() -> float:
    return _value


func set_value(value: float) -> void:
    _value = value


func calc(_ent_stats: Dictionary) -> void:
    return
