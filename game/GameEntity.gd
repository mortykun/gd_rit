class_name GameEntity


var name: String
var gender: String
var age: int = 0
var stats := {}
var levels := {}
var keystones := {}
#var modifiers := {}
var talents := {}
var items := {}
var inventory := {}



func _init():
    pass


func _ready():
    pass


func get_level() -> int:
#    if not stats:
#        return 0

    return 0  # stats.get('level', 0)


func get_starting_stats() -> Dictionary:
    # Get the starting stats for this entity.
    return {}


func get_stat_value(stat_id: String) -> float:
    # Gets the current stat of the entity, if base_only is true, then ignores temporary modifiers.
    var res = stats.get(stat_id)

    if not res:
        return 0.0

    return res.get_value()


func rebuild() -> void:
    _stat_clear()
    _stat_add_base()
    _stat_add_levels()
    _stat_add_gear()
    _stat_add_damage()
    _stat_calc()


func is_player() -> bool:
    return false


func is_npc() -> bool:
    return false


func is_monster() -> bool:
    return false


func is_boss() -> bool:
    return false


func mod_add(stat_grp: String, igm: InstanceGameMod) -> InstanceGameStat:
    var gm : GameMod
    var ist : InstanceGameStat

    gm = FactoryMod.get_mod(igm.mod_id)
    ist = stats.get(gm.stat_id)

    if not ist:
        ist = InstanceGameStat.new()
        ist.initialize(gm.stat_id)
        stats[ist.stat_id] = ist

    ist.mod_add(stat_grp, igm)

    return ist


func mod_add_base(stat_grp: String, stat_id: String, value: Vector2 = Vector2.ZERO) -> InstanceGameStat:
    var igm : InstanceGameMod
    var igs : InstanceGameStat

    igs = stats.get(stat_id)

    if not igs:
        igs = InstanceGameStat.new()
        igs.initialize(stat_id)
        stats[igs.stat_id] = igs

    igm = FactoryMod.new_mod_base(value)

    igs.mod_add(stat_grp, igm)

    return igs


func tracker_add(stat_id: String) -> GameStatTracker:
    var gst := GameStatTracker.new()

    gst.initialize(stat_id)

    stats[stat_id] = gst

    return gst


func _capitalize(sent: String) -> String:
    return sent.substr(0, 1).capitalize() + sent.substr(1)


func _stat_clear():
    # Clear all the stats except the tracker ones.
    var gs: GameStat
    var popped = []

    for stat in FactoryStat.get_stat_group(FactoryStat.Groups.TIER_TRAC):
        gs = stat

        if stats.has(gs.id):
            popped.append(stats[gs.id])

    stats.clear()

    for stat in popped:
        stats[stat.id] = stat


func _stat_add_base():
    var start_stats := get_starting_stats()

    for ss in start_stats:
        mod_add_base(InstanceGameStat.STAT_GRP_BASE, ss, Vector2(start_stats[ss], 0.0))


func _stat_add_levels():
    pass


func _stat_add_gear():
    pass


func _stat_add_damage():
    pass


func _stat_calc():
    _stat_calc_group(FactoryStat.Groups.TIER_TRAC)
    _stat_calc_group(FactoryStat.Groups.TIER_1)
    _stat_calc_group(FactoryStat.Groups.TIER_2)
    _stat_calc_group(FactoryStat.Groups.TIER_3)


func _stat_calc_group(grp_id: int):
    var igs: InstanceGameStat

    for stat in FactoryStat.get_stat_group(grp_id):
        igs = stats.get(stat.id)

        if not igs:
            if stat.tier == GameStat.StatTier.TRACKER:
                tracker_add(stat.id)
                continue
            else:
                igs = mod_add_base(InstanceGameStat.STAT_GRP_BASE, stat.id)

        igs.calc(stats)
