extends Resource

class_name GameMod

enum ModType { BASE, LEVEL, ITEM, IMPLICIT, PREFIX, SUFFIX }
enum ModCalc { ADDED, PERC, SPECIAL }
enum ModScope { LOCAL, GLOBAL }

export(String) var id
export(String) var stat_id
export(ModType) var mod_type
export(ModCalc) var calc
export(ModScope) var scope
export(String) var mod_group
export(String) var item_group
export(PoolStringArray) var required_tags  # required tags an item must have for this mod to be allowed on it
export(PoolStringArray) var any_of_tags    # the item type must any one of these tags
export(int) var calc_idx
export(String) var calc_function
export(Array, Resource) var tiers
