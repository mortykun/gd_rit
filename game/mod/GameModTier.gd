extends Resource

enum ModTier { NONE = 0, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE }

class_name GameModTier

export(int) var level
export(ModTier) var tier
export(String) var affix_flavor
export(int) var rarity = 100
export(int) var sell_value = 1
export(Vector2) var value_from
export(Vector2) var value_to
