extends Node


var _mods: Dictionary = {}
#var _groups: Dictionary = {}
var _item_groups: Dictionary = {}


func _ready():
    if Global.load_recursive_resources(_mods, 'res://game/mod', ['tres']) != OK:
        Global.show_error('Error game modifiers!')

    _init_groups()


func get_mod(id: String) -> GameMod:
    var res: GameMod = _mods.get(id)

    if not res:
        Global.show_error('GameMod not found [%s]' % id)

    return res


func new_mod_raw(mod_id: String, value: Vector2 = Vector2.ZERO) -> InstanceGameMod:
    var gmod := get_mod(mod_id)
    var res := InstanceGameMod.new()

    res.initialize(mod_id, gmod)
    res.value = value

    return res


func new_mod_base(value: Vector2 = Vector2.ZERO) -> InstanceGameMod:
    var gmod := get_mod('base')
    var res := InstanceGameMod.new()

    res.initialize('base', gmod)
    res.value = value

    return res


#func get_mod_group(grp_id: int) -> Array:
#    return _groups[grp_id]


func get_mod_item_group(item_grp_id: String) -> Array:
    return _item_groups[item_grp_id]


#func get_stat_ids_by_type(st: int) -> PoolStringArray:
#    var res := PoolStringArray()
#
#    for gs in _stats.values():
#        if gs.stat_type == st:
#            res.append(gs.id)
#
#    return res
#
#
func _init_groups() -> void:
    # Builds the stat groups
    var gm: GameMod
    var grp: Array

    for mod in _mods.values():
        gm = mod

        if gm.item_group:
            grp = Global.dict_getset_array(_item_groups, gm.item_group)
            grp.append(gm)

#
#
#        if gs.tier == GameStat.StatTier.ONE:
#            item_groups.append(stat)
