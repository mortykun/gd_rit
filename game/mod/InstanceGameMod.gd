# Holds a current mod value

class_name InstanceGameMod


var mod_id: String    # The mod identifier
var value: Vector2    # The current value
var game_mod: GameMod # The associated game mod
var tier_idx: int     # The associated tier index


func _init(init_mod_id: String = ""):
    mod_id = init_mod_id
    tier_idx = -1


func initialize(init_mod_id: String, init_game_mod: GameMod, init_tier_idx: int = -1) -> void:
    mod_id = init_mod_id
    game_mod = init_game_mod
    tier_idx = init_tier_idx


func get_tier() -> GameModTier:
    return game_mod.tiers[tier_idx]


func randomize_tier_value(inp_tier_idx: int = -1):
    if inp_tier_idx >= 0:
        tier_idx = inp_tier_idx

    if tier_idx < 0:
        value = Vector2.ZERO
        return

    if tier_idx >= game_mod.tiers.size():
        Global.show_error("Attmpted to roll tier higher than max tiers! [idx:%d, gm:%s]" % [tier_idx, mod_id])
        return

    var gmt: GameModTier = game_mod.tiers[tier_idx]

    if gmt.value_from.y == 0:
        value.x = gmt.value_from.x
    else:
        value.x = round(Global.roll_frange(gmt.value_from.x, gmt.value_from.y))

    if gmt.value_to.y == 0:
        value.y = gmt.value_to.x
    else:
        value.y = round(Global.roll_frange(gmt.value_to.x, gmt.value_to.y))
