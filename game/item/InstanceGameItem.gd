class_name InstanceGameItem

const COLOR_PROP_INFO = Color(0.8, 0.8, 0.8, 1)
const COLOR_DMG_ATT = COLOR_PROP_INFO
const COLOR_DMG_SPL = Color(0, 0.2, 1, 1)
const COLOR_DMG_NORM = Color(0.9, 0.9, 0.9, 1)
const COLOR_DMG_MAGIC = Color(0.3, 0.3, 1.0, 1)

const CAPTION_COLORS = {
    GameConstants.ItemRarity.NORMAL : Color(0.9, 0.9, 0.9, 1),
    GameConstants.ItemRarity.MAGIC : Color(0.3, 0.3, 1.0, 1),
    GameConstants.ItemRarity.RARE : Color(0.9, 0.9, 0.2, 1),
    GameConstants.ItemRarity.UNIQUE : Color(0.4, 0.4, 0.0, 1),
}


var game_item: GameItem
var item_class: GameItemClass
var item_name: String
var level: int
var rarity: int
var data = {}


func _init(inp_gi: GameItem, inp_gic: GameItemClass, inp_level: int):
    game_item = inp_gi
    item_class = inp_gic
    item_name = inp_gi.item_name
    level = inp_level
    rarity = GameConstants.ItemRarity.NORMAL


func _ready():
    pass


func widget_item_header(lbl_name: RichTextLabel, lbl_class: RichTextLabel) -> void:
    lbl_name.clear()
    lbl_name.push_align(RichTextLabel.ALIGN_CENTER)
    lbl_name.push_color(CAPTION_COLORS[rarity])
    lbl_name.add_text(item_name)
    lbl_name.pop()
    lbl_name.pop()

    if not item_class.ic_name:
        lbl_class.visible = false
        return

    lbl_class.clear()
    lbl_class.push_align(RichTextLabel.ALIGN_CENTER)
    lbl_class.push_color(COLOR_PROP_INFO)
    lbl_class.add_text(item_class.ic_name)
    lbl_class.pop()
    lbl_class.pop()


func widget_base_props(new_prop_lbl: FuncRef) -> void:
    if not data.has('props'):
        return

    var idx = 0
    var lbl: RichTextLabel
    var prop: InstanceGameStat
    var props: Dictionary = data['props']

    for prop_id in GameConstants.PROP_ITEM_ORDER:
        prop = props.get(prop_id)

        if not prop:
            continue

        lbl = new_prop_lbl.call_func(idx)
        idx += 1

        _make_prop_text(lbl, prop)

        if idx >= props.size():
            break

    if idx < props.size():
        Global.show_error("Still have missing props on the item!")


func widget_affix_mods(new_affx_lbl: FuncRef) -> void:
    if not data.has('affx'):
        return

    var idx = 0
    var lbl: RichTextLabel
    var mods: Array = data['affx']

    for mod in mods:
        lbl = new_affx_lbl.call_func(idx)

        idx += 1
        _make_affix_text(lbl, mod)


func widget_image_size() -> Vector2:
    return Global.ITEM_1X1_SIZE * game_item.inv_size


func init_base_props() -> void:
    game_item.init_base_props(Global.dict_getset_array(data, 'base'))


func has_base_stats() -> bool:
    if data.has('base') and data['base'].size() > 0:
        return true

    return false


func has_affixes() -> bool:
    if data.has('affx') and data['affx'].size() > 0:
        return true

    return false


func clear_props() -> void:
    var props = Global.dict_getset_dict(data, 'props')

    props.clear()


func add_prop(igs: InstanceGameStat) -> void:
    var props = Global.dict_getset_dict(data, 'props')
    var pigi: InstanceGameStat = props.get(igs.stat_id)

    if not pigi:
        props[igs.stat_id] = igs
        return

    pigi.stat_add(igs)


func add_prop_for_mod(gm: GameMod, igm: InstanceGameMod) -> void:
    var props = Global.dict_getset_dict(data, 'props')
    var pigi: InstanceGameStat = props.get(gm.stat_id)

    if not pigi:
        pigi = InstanceGameStat.new()
        pigi.initialize(gm.stat_id)
        props[gm.stat_id] = pigi

    pigi.mod_add(InstanceGameStat.STAT_GRP_AFFIX, igm)


func add_affix(gm: GameMod, tier_idx: int) -> void:
    var affxs = Global.dict_getset_array(data, 'affx')
    var igm = InstanceGameMod.new()

    igm.initialize(gm.id, gm, tier_idx)
    igm.randomize_tier_value()
    affxs.append(igm)

    if gm.scope == GameMod.ModScope.LOCAL:
        add_prop_for_mod(gm, igm)


func get_stack_count() -> int:
    if data.has('stack'):
        return data['stack']

    return 1


func set_stack_count(cnt: int) -> void:
    if game_item.stack_size > 1:
        data['stack'] = cnt


func calc_props() -> void:
    if not data.get('props'):
        return

    for prop in data['props'].values():
        prop.calc()

    item_name = game_item.item_name

    if data.has('affx') and data['affx'].size():
        var highest = { 'prefix': null, 'suffix': null}
        var gmt: GameModTier
        var igm: InstanceGameMod

        for mod in data['affx']:
            igm = mod
            gmt = igm.get_tier()

            if not gmt:
                continue

            if igm.game_mod.mod_type == GameMod.ModType.PREFIX:
                if not highest['prefix'] or highest['prefix'].tier < gmt.tier:
                    highest['prefix'] = gmt

            if igm.game_mod.mod_type == GameMod.ModType.SUFFIX:
                if not highest['suffix'] or highest['suffix'].tier < gmt.tier:
                    highest['suffix'] = gmt

        if highest['prefix']:
            item_name = '%s %s' % [highest['prefix'].affix_flavor, item_name]

        if highest['suffix']:
            item_name = '%s %s' % [item_name, highest['suffix'].affix_flavor]

        if rarity == GameConstants.ItemRarity.NORMAL:
            if data['affx'].size() <= 2:
                rarity = GameConstants.ItemRarity.MAGIC
            else:
                rarity = GameConstants.ItemRarity.RARE
        elif rarity == GameConstants.ItemRarity.MAGIC:
            if data['affx'].size() > 2:
                rarity = GameConstants.ItemRarity.RARE


func affix_get_ids() -> Dictionary:
    var res = {}

    if not data.has('affx'):
        return res

    for affx in data['affx']:
        res[affx.mod_id] = null

    return res


func affix_type_count() -> Dictionary:
    var res = {}

    if not has_affixes():
        return res

    var affx: InstanceGameMod

    for affx_obj in data['affx']:
        affx = affx_obj

        if not res.has(affx.game_mod.mod_type):
            res[affx.game_mod.mod_type] = 0

        res[affx.game_mod.mod_type] += 1

    return res


func affix_count() -> int:
    if not has_affixes():
        return 0

    return data['affx'].size()


func _make_prop_text(lbl: RichTextLabel, prop: InstanceGameStat) -> void:
    var gs: GameStat = prop.get_stat()

    lbl.clear()
    lbl.push_align(RichTextLabel.ALIGN_CENTER)

    if gs.stat_type == GameStat.StatType.DAMAGE or\
       gs.stat_type == GameStat.StatType.CRIT or\
       gs.stat_type == GameStat.StatType.DEFENSE:
        if gs.id == 'dmg_att':
            lbl.push_color(COLOR_DMG_ATT)
        elif gs.id == 'dmg_spl':
            lbl.push_color(COLOR_DMG_SPL)
        else:
            lbl.push_color(COLOR_PROP_INFO)

        lbl.add_text(gs.long_name)
        lbl.add_text(": ")
        lbl.pop()

        if prop.has_group_items(InstanceGameStat.STAT_GRP_AFFIX):
            lbl.push_color(COLOR_DMG_MAGIC)
        else:
            lbl.push_color(COLOR_DMG_NORM)

        if gs.stat_type == GameStat.StatType.DAMAGE:
            lbl.add_text(str(prop.get_value_from()))
            lbl.add_text("-")
            lbl.add_text(str(prop.get_value_to()))
        else:
            lbl.add_text(Global.float_2_str_min_decimals(prop.get_value(), gs.max_decimals))

            if gs.is_perc:
                lbl.add_text('%')

        lbl.pop()

    else:
        lbl.push_color(COLOR_DMG_NORM)

        if prop.get_value() >= 0:
            lbl.add_text('+')
            lbl.add_text('%.0f' % [prop.get_value()])
            lbl.add_text(' to ')
        else:
            lbl.add_text('-')
            lbl.add_text('%.0f' % [0 - prop.get_value()])
            lbl.add_text(' from ')

        lbl.add_text(gs.long_name)
        lbl.pop()

    lbl.pop()


func _make_affix_text(lbl: RichTextLabel, igm: InstanceGameMod) -> void:
    var gs: GameStat = FactoryStat.get_stat(igm.game_mod.stat_id)
    var gm: GameMod = igm.game_mod

    lbl.clear()
    lbl.push_align(RichTextLabel.ALIGN_CENTER)
    lbl.push_color(CAPTION_COLORS[GameConstants.ItemRarity.MAGIC])

    if gs.stat_type == GameStat.StatType.DAMAGE:
        if gm.calc == GameMod.ModCalc.ADDED:
            if igm.value.x >= 0:
                lbl.add_text('Adds ')
                lbl.add_text('%.0f to %.0f' % [igm.value.x, igm.value.y])
                lbl.add_text(' to ')
            else:
                lbl.add_text('Removes ')
                lbl.add_text('%.0f to %.0f' % [igm.value.x, igm.value.y])
                lbl.add_text(' from ')

        elif gm.calc == GameMod.ModCalc.PERC:
            if igm.value.x >= 0:
                lbl.add_text(Global.float_2_str_min_decimals(igm.value.x, 0))
                lbl.add_text('% Increased ')
            else:
                lbl.add_text(Global.float_2_str_min_decimals(0 - igm.value.x, 0))
                lbl.add_text('% Decreased ')
        else:
            lbl.add_text(' --TODO-- ')
    else:
        if gm.calc == GameMod.ModCalc.ADDED:
            if igm.value.x >= 0:
                lbl.add_text('+')
                lbl.add_text('%.0f' % [igm.value.x])
                lbl.add_text(' to ')
            else:
                lbl.add_text('-')
                lbl.add_text('%.0f' % [0 - igm.value.x])
                lbl.add_text(' from ')

        elif gm.calc == GameMod.ModCalc.PERC:
            if igm.value.x >= 0:
                lbl.add_text(Global.float_2_str_min_decimals(igm.value.x, 0))
                lbl.add_text('% Increased ')
            else:
                lbl.add_text(Global.float_2_str_min_decimals(0 - igm.value.x, 0))
                lbl.add_text('% Decreased ')
        else:
            lbl.add_text(' --TODO-- ')

    lbl.add_text(gs.long_name)
    lbl.pop()
    lbl.pop()
