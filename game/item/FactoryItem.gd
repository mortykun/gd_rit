extends Node


var _items: Dictionary = {}
var _grouped: Dictionary = {}


func _ready():
    if Global.load_recursive_resources(_items, 'res://game/item', ['tres']) != OK:
        Global.show_error('Error loading items!')

    _group_items()


func get_item(id: String) -> GameItem:
    var res: GameItem = _items.get(id)

    if not res:
        Global.show_error('GameItem not found [%s]' % id)

    return res


func game_item_generate(item_type: String, sub_type: String, level: int) -> InstanceGameItem:
    # Generate a random item.
    # :param item_type: The item type.
    # :param sub_type: The sub item type.
    # :param level: The level of the generated item.
    # :return: The generated item if possible.
    if item_type.empty():
        var it_objs = {}

        for it in GameConstants.ITEM_TYPES:
            it_objs[it] = GameConstants.ITEM_TYPES[it]['drop_chance']

        item_type = Global.roll_weighted(it_objs)

    if sub_type.empty():
        var st_objs = {}
        var it_obj = GameConstants.ITEM_TYPES[item_type]

        for st in it_obj['sub_type']:
            st_objs[st] = it_obj['sub_type'][st]['drop_chance']

        sub_type = Global.roll_weighted(st_objs)

    if not _grouped.has(item_type):
        return null

    if not _grouped[item_type].has(sub_type):
        return null

    var item_list = _grouped[item_type][sub_type]
    var item_objs = {}

    for it in item_list:
        if it.level <= level:
            item_objs[it.id] = 100

    if item_objs.empty():
        return null

    return game_item_new(Global.roll_weighted(item_objs), level)


func game_item_new(id: String, level: int) -> InstanceGameItem:
    # Create a new instance of a game item
    var gi := get_item(id)
    var gic := FactoryItemClass.get_item_class(gi.item_class)
    var res := InstanceGameItem.new(gi, gic, level)

    res.init_base_props()

    game_item_calc(res)

    return res


func game_item_calc(igi: InstanceGameItem) -> void:
    # Calc all the stats and modifiers on an item.
    if not igi.has_base_stats() and not igi.has_affixes():
        return

    var base_stats = igi.data.get(InstanceGameStat.STAT_GRP_BASE)
    var igs: InstanceGameStat
    var igm: InstanceGameMod

    igi.clear_props()

    if base_stats:
        for bs in base_stats:
            for stat_key in bs:
                igs = InstanceGameStat.new()
                igm = FactoryMod.new_mod_base(bs[stat_key])

                igs.initialize(stat_key)
                igs.mod_add(InstanceGameStat.STAT_GRP_BASE, igm)

                igi.add_prop(igs)

    igi.calc_props()


func make_random_rarity(igi: InstanceGameItem, magic_chance: float, rare_chance: float) -> void:
    if igi.rarity != GameConstants.ItemRarity.NORMAL:
        return

    var roll = Global.roll_frange(0.0, 100.0)

    if roll > magic_chance:
        return

    roll = Global.roll_frange(0.0, 100.0)

    if roll > rare_chance:
        make_magic(igi)
    else:
        make_rare(igi)


func make_magic(igi: InstanceGameItem) -> void:
    if igi.rarity != GameConstants.ItemRarity.NORMAL:
        Global.show_error("Cannot make a non normal item magical [%s]" % igi)
        return

    affix_add(igi)

    if Global.roll_coinflip():
        affix_add(igi)


func make_rare(igi: InstanceGameItem) -> void:
    if igi.rarity != GameConstants.ItemRarity.NORMAL and igi.rarity != GameConstants.ItemRarity.MAGICAL:
        Global.show_error("Cannot make a non normal/magic item rare [%s]" % igi)
        return

    var affixes = Global.roll_irange(4, 6)

    if igi.rarity == GameConstants.ItemRarity.MAGIC:
        affixes -= igi.affix_count()

    while affixes > 0:
        if not affix_add(igi):
            break
        affixes -= 1


func affix_add(igi: InstanceGameItem) -> bool:
    # Adds an affix to a game item.
    # :param igi: The item to  add the affix to.
    # :return: True if the affix was a added
    if igi.rarity == GameConstants.ItemRarity.UNIQUE:
        Global.show_error("Cannot add affixes to unique items! [%s]" % igi)
        return false

    var atc := igi.affix_type_count()
    var allow_prefix = true
    var allow_suffix = true
    var allow_types = []
    var max_affix = GameConstants.GAME_ITEM_MAX_MAGIC_AFFIX

    if igi.rarity == GameConstants.ItemRarity.RARE:
        max_affix = GameConstants.GAME_ITEM_MAX_RARE_AFFIX

    if atc.get(GameMod.ModType.PREFIX, 0) >= max_affix:
        allow_prefix = false

    if atc.get(GameMod.ModType.SUFFIX, 0) >= max_affix:
        allow_suffix = false

    if not allow_prefix and not allow_suffix:
        if igi.rarity == GameConstants.ItemRarity.MAGIC:
            allow_prefix = true
            allow_suffix = true
            max_affix = GameConstants.GAME_ITEM_MAX_RARE_AFFIX
        else:
            Global.show_error("Cannot add affixes, max prefix and suffix level reached! [%d]" % max_affix)
            return false

    if allow_prefix:
        allow_types.append(GameMod.ModType.PREFIX)

    if allow_suffix:
        allow_types.append(GameMod.ModType.SUFFIX)

    var item_mods := FactoryMod.get_mod_item_group(igi.item_class.mod_item_group)

    if not item_mods:
        Global.show_error("No item mods found for item [class:%s, mod_item_group:%s]" % [igi.item_class.id, igi.item_class.mod_item_group])
        return false

    var roll_mods := _all_mod_roll_tiers(igi, item_mods, allow_types)

    if not roll_mods:
        Global.show_error("No avialable mods for the item.")
        return false

    var roll := Global.roll_weight_start()
    var concl

    for xm in roll_mods:
        Global.roll_weight_append(roll, xm, xm['gmt'].rarity)

    concl = Global.roll_weight_conclude(roll)

    if not concl:
        Global.show_error("Unable to conclude the mod roll!")
        return false

    igi.add_affix(concl["gm"], concl["idx"])
    igi.calc_props()

    return true


func _all_mod_roll_tiers(igi: InstanceGameItem, item_mods: Array, allow_types: Array) -> Array:
    var res = []
    var gm: GameMod
    var gmt: GameModTier
    var affx_ids := igi.affix_get_ids()
    var ok: bool
    var idx: int

    for imod in item_mods:
        gm = imod

        if affx_ids.has(gm.id):
            continue

        if not gm.mod_type in allow_types:
            continue

        if gm.required_tags:
            ok = true

            for tag in gm.required_tags:
                if not tag in igi.item_class.tags:
                    ok = false
                    break

            if not ok:
                continue

        if gm.any_of_tags:
            ok = false

            for tag in igi.item_class.tags:
                if tag in gm.any_of_tags:
                    ok = true
                    break

            if not ok:
                continue

        idx = 0

        for tier in gm.tiers:
            gmt = tier

            if gmt.level > igi.level:
                break

            res.append({"gm": gm, "gmt": gmt, "idx": idx})
            idx += 1

    return res


func _group_items() -> void:
    _grouped.clear()

    var item_group: Dictionary
    var item_list: Array

    for item in _items.values():
        item_group = Global.dict_getset_dict(_grouped, item.item_type)
        item_list = Global.dict_getset_array(item_group, item.item_sub_type)

        item_list.append(item)
