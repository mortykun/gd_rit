extends Resource

class_name GameItem


export(String) var id
export(String) var item_name
export(String) var item_class
export(String) var item_type
export(String) var item_sub_type
export(int) var level = 1
export(int) var stack_size = 1
export(Texture) var inv_img
export(Vector2) var inv_size = Vector2(1, 1)
export(PoolStringArray) var implicit_local
export(PoolStringArray) var implicit_global

func init_base_props(dest: Array) -> void:
    var prop: Dictionary

    for imp_loc in implicit_local:
        prop = create_implicit_prop(imp_loc)
        dest.append(prop)

    for imp_glb in implicit_global:
        prop = create_implicit_prop(imp_glb)
        dest.append(prop)


func create_implicit_prop(impl: String) -> Dictionary:
    # Reads an implicit property string, and calculates the value
    var res = {}

    var parts := impl.split(':')
    var vals := parts[1].split('-')
    var val = Global.roll_irange(int(vals[0]), int(vals[1]))

    res[parts[0]] = Vector2(val, val)

    return res

func sfx_drop():
    GlobalSfx.sfx("sfx_opt_change")


func sfx_pickup():
    GlobalSfx.sfx("sfx_opt_change")
