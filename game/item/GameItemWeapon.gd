extends GameItem

class_name GameItemWeapon

export (GameConstants.DamageType) var damage_type
export (bool) var is_two_handed = false
export (Vector2) var att_damage
export (Vector2) var spl_damage
export (float) var base_crit


func init_base_props(dest: Array) -> void:
    if att_damage != Vector2.ZERO:
        dest.append({'dmg_att': att_damage})

    if spl_damage != Vector2.ZERO:
        dest.append({'dmg_spl': spl_damage})

    dest.append({'crit_ch': Vector2(base_crit, 0.0)})
