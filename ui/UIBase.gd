extends Control

class_name UIBase


func _ready():
    init_ui()


func init_ui(_params = null):
    pass


func toggle_ui(ui_name: String, visibile: bool, params = null, force_init: bool = false) -> void:
    var ui_area: UIArea = get_parent()

    ui_area.toggle_ui(ui_name, visibile, params, force_init)



func change_ui(ui_name: String, params = null) -> void:
    var ui_area: UIArea = get_parent()

    GlobalSfx.sfx_menu_select()

    ui_area.show_ui(ui_name, params)
