extends Control

class_name UIArea

var _curr_ui: Control = null

func _ready():
    rescale()

    for ui_ctrl in get_children():
        ui_ctrl.visible = false
#        ui_ctrl.connect("goto_ui", self, "show_ui")


func rescale() -> void:
    var safe_rect := OS.get_window_safe_area()
    var screen_size := OS.get_window_size()
    var layer: CanvasLayer = get_node("..")
    var scale = safe_rect.size / screen_size

    layer.transform = layer.transform.scaled(scale)
    layer.transform = layer.transform.translated(safe_rect.position)


func hide_ui() -> void:
    if _curr_ui:
        _curr_ui.visible = false
        _curr_ui = null


func show_ui(ui_name: String, params = null) -> void:
    if not ui_name:
        hide_ui()
        return

    var to_show: Control = get_node(ui_name)

    if not to_show:
        Global.show_error("UI [%s] not found!" % ui_name)
        return

    if _curr_ui:
        _curr_ui.visible = false

    _curr_ui = to_show
    _curr_ui.visible = true
    _curr_ui.init_ui(params)


func toggle_ui(ui_name: String, visible: bool, params = null, force_init: bool = false) -> void:
    var to_toggle: Control = get_node(ui_name)

    if not to_toggle:
        Global.show_error("UI [%s] not found - cannot toggle!!" % ui_name)
        return

    if visible:
        if to_toggle.visible:
            if force_init:
                to_toggle.init_ui(params)
        else:
            to_toggle.visible = true
            to_toggle.init_ui(params)
    else:
        to_toggle.visible = false
