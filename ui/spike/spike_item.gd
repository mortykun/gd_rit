extends Control

onready var drop_type := $panel_opts/vbox/drop_item_type
onready var drop_sub := $panel_opts/vbox/drop_item_sub_type
onready var drop_item := $panel_opts/vbox/drop_item
onready var drop_rare := $panel_opts/vbox/drop_rarity
onready var item_info := $ItemInfoWidget
onready var item_level := $panel_opts/vbox/item_level
onready var item_inv := $InvStashWidget
onready var item_show := $ItemShowWidget


var _igi: InstanceGameItem


func _ready():
    item_info.hide()
    item_show.hide()
    MouseState.item_img = $item_mouse_cursor

    drop_type.add_item("<RANDOM>", -1)
    drop_rare.add_item("<RANDOM>", -1)
    drop_rare.add_item("normal")
    drop_rare.add_item("magic")
    drop_rare.add_item("rare")

    var itype_keys = []
    var it_rec: Dictionary

    for itk in GameConstants.ITEM_TYPES:
        it_rec = GameConstants.ITEM_TYPES[itk]

        if it_rec.get("drop_chance", 0) < 1:
            continue

        itype_keys.append(itk)

    itype_keys.sort()

    for x in itype_keys:
        drop_type.add_item(x)

    item_inv.set_item_info_widgets(item_show)

    for slot_node in $WornItems.get_children():
        if slot_node.is_in_group("inventory_container"):
            slot_node.set_item_info_widgets(item_show)


func _on_btn_back_pressed():
    MouseState.clear_state()
    SceneChanger.main_menu()


func _on_btn_add_affix_pressed():
    if not _igi:
        return

    FactoryItem.affix_add(_igi)
    item_info.show_item(_igi)


func _reload_sub_types() -> void:
    drop_sub.selected = 0
    drop_sub.clear()
    drop_sub.disabled = true
    drop_sub.text = '<NOT APPLICABLE>'

    _reload_items()

    var st_rec = GameConstants.ITEM_TYPES.get(drop_type.text)

    if not st_rec:
        return

    drop_sub.add_item('<RANDOM>', -1)

    var it_dc: int = st_rec.get('drop_chance', 0)
    var st_dc: int

    for st_id in st_rec['sub_type']:
        st_dc = st_rec['sub_type'][st_id].get('drop_chance', it_dc)

        if st_dc < 1:
            continue

        drop_sub.add_item(st_id)

    if drop_sub.items.size() < 1:
        return

    drop_sub.selected = -1
    drop_sub.disabled = false


func _reload_items() -> void:
    drop_item.selected = 0
    drop_item.clear()
    drop_item.disabled = true
    drop_item.text = '<NOT APPLICABLE>'

    if drop_sub.selected < 1:
        return

    drop_item.add_item('<RANDOM>', -1)

    var ilist = []

    for item in FactoryItem._grouped[drop_type.text][drop_sub.text]:
        ilist.append(item.id)

    if not ilist:
        return

    ilist.sort()

    for item in ilist:
        drop_item.add_item(item)

    drop_item.selected = -1
    drop_item.disabled = false


func _on_btn_recalc_pressed():
    if not _igi:
        return

    _igi.calc_props()
    item_info.show_item(_igi)


func _on_btn_to_inventory_pressed():
    if not _igi:
        return

    if item_inv.item_try_add(_igi):
        item_info.hide()
        _igi = null
    else:
        Global.show_error("Could not find a spot for the item.")


func _on_btn_inv_clear_pressed():
    item_inv.items_clear()


func _on_btn_gen_item_pressed():
    if drop_item.selected > 0:
        _igi = FactoryItem.game_item_new(drop_item.text, item_level.value)
    else:
        var item_type = ""
        var sub_type = ""

        if drop_type.selected > 0:
            item_type = drop_type.text

        if drop_sub.selected > 0:
            sub_type = drop_sub.text

        _igi = FactoryItem.game_item_generate(item_type, sub_type, item_level.value)

    if not _igi:
        return

    if drop_rare.selected < 1:
        FactoryItem.make_random_rarity(_igi, 30.0, 30.0)
    elif drop_rare.text == "magic":
        FactoryItem.make_magic(_igi)
    elif drop_rare.text == "rare":
        FactoryItem.make_rare(_igi)

    item_info.show_item(_igi)


func _on_drop_item_type_item_selected(_index):
    _reload_sub_types()


func _on_drop_item_sub_type_item_selected(_index):
    _reload_items()
