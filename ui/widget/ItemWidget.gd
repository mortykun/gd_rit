tool
extends TextureRect

# NOTE!
# For now, we are not going to support drag and drop for any inventory items. This because
#  there is an outstanding issue where Godot does not support mouse events while any mouse
#  button is being dragged. This issue has been around for like 3 years now without
#  it being addressed. I see there is some talk of a fix in 3.4 or 3.5 but fuck that. I am not
#  going break my balls dealing with it now, only to have to re-implement it proplerly when
#  the godot devs decided to merge in the pull request that fixes it.

class_name ItemWidget

onready var bg_col = $bg_col

var item: InstanceGameItem setget _item_set
var inv_rect: Rect2


func _init(init_item: InstanceGameItem = null):
    item = init_item
    inv_rect = Rect2(Vector2.ZERO, Vector2.ZERO)
    rect_min_size = Global.ITEM_1X1_SIZE
    rect_size = rect_min_size


func _ready():
    bg_col.visible = false


func _item_set(value: InstanceGameItem) -> void:
    item = value
    show_item()


func set_inventory_position(value: Vector2) -> void:
    inv_rect = Rect2(value, item.game_item.inv_size)


func show_item() -> void:
    if not item:
        if bg_col:
            bg_col.visible = false
        visible = false
        return

    texture = item.game_item.inv_img
    set_size(item.widget_image_size())

    if bg_col:
        bg_col.visible = false

    visible = true


func clear_highlight() -> void:
    bg_col.visible = false


func set_highlight(col: Color) -> void:
    bg_col.color = col
    bg_col.show()


func inventory_or_slot_return() -> void:
    bg_col.hide()
    modulate.a = 1.0


func parent_object() -> Node:
    var par = get_parent()

    while par:
        if par.is_in_group("inventory_container"):
            return par

        par = par.get_parent()

    return null


func parent_remove_from() -> void:
    var par = parent_object()

    if par:
        par.item_widget_remove(self)


func parent_show_info() -> void:
    var par = parent_object()

    if par:
        par.item_info_show(self)


func parent_hide_info() -> void:
    var par = parent_object()

    if par:
        par.item_info_hide()


func pickup_item_temporary() -> void:
    MouseState.item_on_cursor = self
    bg_col.visible = true
    bg_col.color = GameConstants.ITEM_WIDGET_COLOR_HIGHLIGHT
    modulate.a = 0.5
    parent_hide_info()


func pickup_item_permenant() -> void:
    parent_remove_from()
    MouseState.item_on_cursor = self
    bg_col.visible = false
    modulate.a = 1.0


func drop_item_temporary() -> void:
    if not get_parent_control():
        return

    MouseState.item_on_cursor = null
    inventory_or_slot_return()

    if MouseState.item_under_cursor:
        MouseState.item_under_cursor.clear_highlight()

func sfx_drop() -> void:
    if item:
        item.game_item.sfx_drop()

func _on_ItemWidget_mouse_entered():
    MouseState.item_under_cursor = self

    if MouseState.has_item_on_cursor():
        parent_show_info()
    else:
        bg_col.visible = true
        bg_col.color = GameConstants.ITEM_WIDGET_COLOR_HIGHLIGHT
        parent_show_info()


func _on_ItemWidget_mouse_exited():
    MouseState.item_under_cursor = null

    if MouseState.item_on_cursor != self:
        bg_col.visible = false

    parent_hide_info()
