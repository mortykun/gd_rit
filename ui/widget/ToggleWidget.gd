extends Panel

class_name ToggleWidget

signal toggle_selected
signal value_changed

export (String) var widget_tag

func _ready():
    pass


func initialize(label: String, checked: bool) -> void:
    $lbl_name.text = label
    $check_btn.pressed = checked


func _on_check_btn_pressed():
    GlobalSfx.sfx_opt_change()
    emit_signal("value_changed", self, $check_btn.pressed)


func _on_ToggleWidget_focus_entered():
    emit_signal("toggle_selected", self)
    $highlight.visible = true


func _on_ToggleWidget_focus_exited():
    $highlight.visible = false


func _on_check_btn_focus_entered():
    emit_signal("toggle_selected", self)
    $highlight.visible = true


func _on_check_btn_focus_exited():
    $highlight.visible = false
