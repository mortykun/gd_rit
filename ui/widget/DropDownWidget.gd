extends Panel

class_name DropDownWidget

signal dropdown_selected
signal value_changed


func _ready():
    pass


func initialize(label: String, options: PoolStringArray, selected: int = 0) -> void:
    $lbl_name.text = label

    $btn_options.items.clear()

    for item in options:
        $btn_options.add_item(item)

    $btn_options.selected = selected
    $btn_options.text = options[selected]


func get_option_id() -> int:
    return $btn_options.selected


func get_option_text() -> String:
    return $btn_options.get_item_text($btn_options.selected)


func _on_btn_options_item_selected(index):
    GlobalSfx.sfx_opt_change()
    emit_signal("value_changed", self, index)


func _on_DropDownWidget_focus_entered():
    emit_signal("dropdown_selected", self)
    $highlight.visible = true


func _on_DropDownWidget_focus_exited():
   $highlight.visible = false


func _on_btn_options_focus_entered():
    emit_signal("dropdown_selected", self)
    $highlight.visible = true


func _on_btn_options_focus_exited():
   $highlight.visible = false
