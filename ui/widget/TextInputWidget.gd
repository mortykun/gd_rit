extends Panel

class_name TextInputWidget

signal textinput_selected
signal value_changed


func _ready():
    pass


func initialize(label: String, cur_text: String) -> void:
    $lbl_name.text = label
    $input.text = cur_text


func get_text() -> String:
    return $input.text


func _on_input_text_changed(new_text):
    emit_signal("value_changed", self, new_text)


func _on_TextInputWidget_focus_entered():
    emit_signal("textinput_selected", self)
    $highlight.visible = true


func _on_TextInputWidget_focus_exited():
   $highlight.visible = false


func _on_input_focus_entered():
    emit_signal("textinput_selected", self)
    $highlight.visible = true


func _on_input_focus_exited():
   $highlight.visible = false
