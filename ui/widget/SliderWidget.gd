extends Panel

class_name SliderWidget

signal slider_selected
signal value_changed

export (String) var widget_tag

func _ready():
    pass


func initialize(label: String, min_val: int, max_val: int, cur_val: int) -> void:
    $lbl_name.text = label
    $slider.min_value = min_val
    $slider.max_value = max_val
    $slider.value = cur_val


func _on_slider_value_changed(value):
    GlobalSfx.sfx_opt_change()
    emit_signal("value_changed", self, value)


func _on_SliderWidget_focus_entered():
    emit_signal("slider_selected", self)
    $highlight.visible = true


func _on_SliderWidget_focus_exited():
   $highlight.visible = false


func _on_slider_focus_entered():
    emit_signal("slider_selected", self)
    $highlight.visible = true


func _on_slider_focus_exited():
   $highlight.visible = false
