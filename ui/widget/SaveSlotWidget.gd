extends Panel

class_name SaveSlotWidget

signal slot_selected

export(int) var slot_id


func _ready():
    pass


func initialize(slot_header: Dictionary) -> void:
    if slot_header.size() < 1:
        $lbl_name.text = '<%3d EMPTY>' % slot_id
        $lbl_date.visible = false
    else:
        var dt := OS.get_datetime_from_unix_time(slot_header['date_saved'])

        $lbl_name.text = slot_header['caption']
        $lbl_date.text =  Global.datetime_to_string(dt)
        $lbl_date.visible = true


func _on_SaveSlotWidget_focus_entered():
    emit_signal("slot_selected", slot_id)
    $highlight.visible = true


func _on_SaveSlotWidget_focus_exited():
    $highlight.visible = false
