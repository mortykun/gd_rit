extends PanelContainer

class_name ItemInfoWidget

const INFO_PADDING = 5

onready var lbl_name := $vbox/header/lbl_item_name
onready var lbl_class := $vbox/header/lbl_item_class
onready var header_vbox := $vbox/header
onready var props_vbox := $vbox/props
onready var image_vbox := $vbox/image
onready var image := $vbox/image/image
onready var affix_vbox := $vbox/affix
onready var flav_vbox := $vbox/flav

var _igi: InstanceGameItem


func _ready():
    rect_min_size.x = Global.ITEM_1X1_SIZE.x * 5
    rect_min_size.y = 50

    _hide_prop()
    _hide_affix()
    _hide_flavor()


func show_item(igi: InstanceGameItem,
               inv_global_rect: Rect2 = Rect2(Vector2.ZERO, Vector2.ZERO),
               incl_img: bool = true,
               incl_flav: bool = true):
    hide() # Else the below resizing code has no effect
    rect_size = rect_min_size
    _igi = igi

    _hide_prop()
    _hide_image()
    _hide_affix()
    _hide_flavor()

    igi.widget_item_header(lbl_name, lbl_class)
    igi.widget_base_props(funcref(self, "_base_prop_label"))
    igi.widget_affix_mods(funcref(self, "_affix_mod_label"))

    if incl_img:
        _show_image()

    if incl_flav:
        _show_flavor()

    show()

    if inv_global_rect.position != Vector2.ZERO:
        _set_info_position(inv_global_rect)


func _show_image():
    if not _igi.game_item.inv_img or _igi.game_item.inv_size == Vector2.ZERO:
        return

    image_vbox.visible = true

    image.rect_size = _igi.game_item.inv_size * Global.ITEM_1X1_SIZE
    image.rect_min_size = image.rect_size
    image.texture = _igi.game_item.inv_img


func _show_flavor():
    pass


func _base_prop_label(prop_idx: int) -> RichTextLabel:
    props_vbox.visible = true

    var res: RichTextLabel

    if prop_idx < props_vbox.get_child_count():
        res = props_vbox.get_child(prop_idx)
    else:
        res = props_vbox.get_child(0).duplicate()
        props_vbox.add_child(res)

    res.visible = true

    return res


func _affix_mod_label(affx_idx: int) -> RichTextLabel:
    affix_vbox.visible = true
    affix_vbox.get_child(0).visible = true

    var res: RichTextLabel

    if affx_idx + 1 < affix_vbox.get_child_count():
        res = affix_vbox.get_child(affx_idx + 1)
    else:
        res = affix_vbox.get_child(1).duplicate()
        affix_vbox.add_child(res)

    res.visible = true

    return res


func _hide_prop():
    props_vbox.visible = false

    for node in props_vbox.get_children():
        node.visible = false


func _hide_image():
    image_vbox.visible = false
    image.rect_min_size = Vector2.ZERO
    image.rect_size = Vector2.ZERO
    image.texture = null


func _hide_affix():
    affix_vbox.visible = false

    for node in affix_vbox.get_children():
        node.visible = false


func _hide_flavor():
    flav_vbox.visible = false


func _set_info_position(global_inv_pos: Rect2) -> void:
    var screen_size := Vector2(ProjectSettings.get_setting('display/window/size/width'), ProjectSettings.get_setting('display/window/size/height'))
    var new_pos = Vector2()

    new_pos.y = global_inv_pos.position.y
    new_pos.x = global_inv_pos.position.x + global_inv_pos.size.x + INFO_PADDING

    if new_pos.x + rect_size.x > screen_size.x:
        new_pos.x = global_inv_pos.position.x - rect_size.x - INFO_PADDING

    if new_pos.y + rect_size.y > screen_size.y:
        new_pos.y = screen_size.y - (rect_size.y)

    set_global_position(new_pos)
