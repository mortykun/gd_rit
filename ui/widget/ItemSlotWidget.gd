tool
extends Control

class_name ItemSlotWidgets

export (Vector2) var slot_size
export (Texture) var slot_bg
export (GameConstants.ItemSlot) var item_slot

onready var _item_node := $container/item
onready var _item_col := $container/item_status_col

var _content_item: ItemWidget
var _show_widget: ItemInfoWidget


func _ready():
    _item_col.hide()
#    _resize_slot()

    if slot_bg:
        $container/bg_img.texture = slot_bg


func set_item_info_widgets(show_widget: ItemInfoWidget) -> void:
    _show_widget = show_widget


func item_widget_add(iwidg: ItemWidget, be_quiet: bool = false) -> void:
    if iwidg.item.game_item.inv_size == slot_size:
        iwidg.set_position(Vector2.ZERO)

    var iwidg_pos = Vector2()
    var iwidg_size := iwidg.rect_size
    var slot_pixel_size = rect_size

    iwidg_pos.x = (slot_pixel_size.x * 0.5) - (iwidg_size.x * 0.5)
    iwidg_pos.y = (slot_pixel_size.y * 0.5) - (iwidg_size.y * 0.5)

    _content_item = iwidg

    _item_node.add_child(iwidg)
    iwidg.set_position(iwidg_pos)
    iwidg.inventory_or_slot_return()

    if not be_quiet:
        iwidg.sfx_drop()


func item_info_show(widg: ItemWidget) -> void:
    if _show_widget:
        _show_widget.show_item(widg.item,
                               widg.get_global_rect(),
                               false,  # hide image
                               true)   # show flavor text


func item_info_hide() -> void:
    if _show_widget:
        _show_widget.visible = false
        _show_widget.hide()


func item_widget_clear() -> bool:
    _content_item = null

    if _item_node.get_child_count():
        var tmp = _item_node.get_children()

        for child in tmp:
            _item_node.remove_child(child)

        return true

    return false


func item_widget_remove(iwidg: ItemWidget) -> bool:
    _content_item = null

    if _item_node.get_child(0) == iwidg:
        _item_node.remove_child(iwidg)
        return true

    return false


func can_drop_item(item: ItemWidget) -> bool:
    var item_type = item.item.game_item.item_type

    if item_slot == GameConstants.ItemSlot.ON_HAND:
        if item_type == GameConstants.ITEM_TYPE_WEAPON:
            return true

        return false

    if item_slot == GameConstants.ItemSlot.OFF_HAND:
        if item_type == GameConstants.ITEM_TYPE_WEAPON:
            return true

        if item_type == GameConstants.ITEM_TYPE_SHIELD:
            return true

        if item_type == GameConstants.ITEM_TYPE_QUIVER:
            return true

        return false

    if item_slot == GameConstants.ItemSlot.GLOVES:
        return item_type == GameConstants.ITEM_TYPE_GLOVES

    if item_slot == GameConstants.ItemSlot.BOOTS:
        return item_type == GameConstants.ITEM_TYPE_BOOTS

    if item_slot == GameConstants.ItemSlot.HELM:
        return item_type == GameConstants.ITEM_TYPE_HELM

    if item_slot == GameConstants.ItemSlot.AMULET:
        return item_type == GameConstants.ITEM_TYPE_AMULET

    if item_slot == GameConstants.ItemSlot.LEFT_FINGER or item_slot == GameConstants.ItemSlot.RIGHT_FINGER:
        return item_type == GameConstants.ITEM_TYPE_RING

    return false


func set_highlight(col: Color) -> void:
    _item_col.color = col
    _item_col.show()


func _resize_slot() -> void:
    pass
#    var game_inv_size := Global.ITEM_1X1_SIZE
#    var pixel_size = Vector2()

#    pixel_size.x = slot_size.x * game_inv_size.x
#    pixel_size.y = slot_size.y * game_inv_size.y
#
#    if rect_size != pixel_size:
#        rect_size = pixel_size


func _clear_highlight() -> void:
    _item_col.hide()


func _handle_input(event: InputEvent) -> void:
    if event is InputEventMouseButton:
        _handle_input_mouse_click(event)

    accept_event()


func _handle_input_mouse_click(event: InputEventMouseButton) -> void:
    if event.pressed:
        return

    if event.button_index == BUTTON_LEFT:
        if MouseState.item_on_cursor:
            if not can_drop_item(MouseState.item_on_cursor):
                return

            if MouseState.item_under_cursor == MouseState.item_on_cursor:
                MouseState.item_on_cursor.drop_item_temporary()
                XGame.player_equip_item(item_slot, MouseState.item_on_cursor)
                return

            if _content_item:
                var tmp_widg = MouseState.item_on_cursor
                MouseState.item_on_cursor = null
                tmp_widg.parent_remove_from()
                _content_item.pickup_item_permenant()
                item_widget_add(tmp_widg)
                MouseState.item_under_cursor = tmp_widg
            else:
                MouseState.item_on_cursor.parent_remove_from()
                item_widget_add(MouseState.item_on_cursor)
                MouseState.item_on_cursor = null

            if _content_item:
                XGame.player_equip_item(item_slot, _content_item)
                item_info_show(_content_item)
                _content_item.set_highlight(GameConstants.ITEM_WIDGET_COLOR_HIGHLIGHT)

        else:
            if MouseState.item_under_cursor:
                MouseState.item_under_cursor.pickup_item_temporary()

    elif event.button_index == BUTTON_RIGHT:
        if MouseState.item_on_cursor:
            MouseState.item_on_cursor.drop_item_temporary()
            _clear_highlight()



func _on_container_gui_input(event):
    if not event.is_echo():
        _handle_input(event)


func _on_container_mouse_entered():
    if MouseState.item_on_cursor:
        if can_drop_item(MouseState.item_on_cursor):
            if _content_item:
                set_highlight(GameConstants.ITEM_WIDGET_COLOR_OK)
            else:
                set_highlight(GameConstants.ITEM_WIDGET_COLOR_HIGHLIGHT)

        return


func _on_container_mouse_exited():
    _clear_highlight()
