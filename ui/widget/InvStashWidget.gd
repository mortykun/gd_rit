tool
extends Control

class_name InvStashWidget

const ITEM_WIDGET_SCENE = preload("res://ui/widget/ItemWidget.tscn")

export (Vector2) var inventory_size = Vector2(6, 6)

onready var _items_node := $container/items
onready var _item_place := $item_place_col

var _content: Dictionary = {}
var _show_widget: ItemInfoWidget


func _ready():
    _item_place.hide()
    _item_place.color = GameConstants.ITEM_WIDGET_COLOR_OK
    _resize_grid()


func set_item_info_widgets(show_widget: ItemInfoWidget) -> void:
    _show_widget = show_widget


func items_clear():
    _content.clear()

    for child in _items_node.get_children():
        child.free()

func item_info_show(widg: ItemWidget) -> void:
    if _show_widget:
        _show_widget.show_item(widg.item,
                               widg.get_global_rect(),
                               false,  # hide image
                               true)   # show flavor text


func item_info_hide() -> void:
    if _show_widget:
        _show_widget.hide()


func item_add(igi: InstanceGameItem, pos: Vector2) -> void:
    var iwidg := ITEM_WIDGET_SCENE.instance()

    iwidg.item = igi
    iwidg.set_inventory_position(pos)
    iwidg.set_position(pos * Global.ITEM_1X1_SIZE)
    _content[iwidg] = null
    _items_node.add_child(iwidg)


func item_widget_add(iwidg: ItemWidget, pos: Vector2) -> void:
    iwidg.set_inventory_position(pos)
    iwidg.set_position(pos * Global.ITEM_1X1_SIZE)
    _content[iwidg] = null
    _items_node.add_child(iwidg)
    iwidg.inventory_or_slot_return()
    iwidg.sfx_drop()


func item_widget_remove(iwidg: ItemWidget) -> bool:
    _items_node.remove_child(iwidg)
    return _content.erase(iwidg)


func item_find_spot(igi: InstanceGameItem) -> Vector2:
    var item_rect := Rect2(Vector2.ZERO, igi.game_item.inv_size)
    var idx_y: int = 0
    var idx_x: int = 0
    var items := _content.keys()
    var fnd: bool

    while idx_x < inventory_size.x:
        idx_y = 0
        item_rect.position.y = idx_y

        while idx_y < inventory_size.y:
            fnd = true

            for item in items:
                if item_rect.intersects(item.inv_rect, false):
                    fnd = false
                    break

            if fnd:
                return item_rect.position

            idx_y += 1
            item_rect.position.y = idx_y

            if item_rect.position.y + item_rect.size.y > inventory_size.y:
                break

        idx_x += 1
        item_rect.position.x = idx_x

        if item_rect.position.x + item_rect.size.x > inventory_size.x:
            break

    return Vector2.INF


func item_try_add(igi: InstanceGameItem) -> bool:
    var pos := item_find_spot(igi)

    if pos == Vector2.INF:
        return false

    item_add(igi, pos)

    return true


func _handle_input(event: InputEvent) -> void:
    if event is InputEventMouseMotion:
        _handle_input_mouse_move(event)

    if event is InputEventMouseButton:
        _handle_input_mouse_click(event)

    accept_event()


func _determine_items_under_rect(src_rect: Rect2) -> Array:
    var res = []

    for item in _content.keys():
        if item == MouseState.item_on_cursor:
            continue

        if src_rect.intersects(item.inv_rect):
            res.append(item)

    return res


func _determine_inv_state_under_cursor(mouse_pos: Vector2, item_on_cursor: ItemWidget) -> Dictionary:
    var res = {}

    var relative_position := mouse_pos - rect_global_position
    var gi : GameItem = item_on_cursor.item.game_item
    var item_size := item_on_cursor.item.widget_image_size()
    var item_pos = Vector2(relative_position.x - (item_size.x * 0.5), relative_position.y - (item_size.y * 0.5))

    _item_place.rect_position = item_pos
    _item_place.rect_size = item_size
    _item_place.show()

    item_pos = (_item_place.rect_position / Global.ITEM_1X1_SIZE).round()

    if item_pos.x < 0:
        item_pos.x = 0
    elif item_pos.x + gi.inv_size.x > inventory_size.x:
        item_pos.x = inventory_size.x - gi.inv_size.x

    if item_pos.y < 0:
        item_pos.y = 0
    elif item_pos.y + gi.inv_size.y > inventory_size.y:
        item_pos.y = inventory_size.y - gi.inv_size.y

    _item_place.rect_position = item_pos * Global.ITEM_1X1_SIZE

    res['item_pos'] = item_pos
    res['items_under'] = _determine_items_under_rect(Rect2(item_pos, gi.inv_size))
    res['can_swop'] = false
    res['single_item'] = null

    if res['items_under'].size() == 1:
        var item = res['items_under'][0]
        res['single_item'] = item

        if item.inv_rect.size <= item_size:
            res['can_swop'] = true

#    $Label.text = JSON.print(res, "  ", true)

    return res


func _handle_input_mouse_move(event: InputEventMouseMotion) -> void:
    if not MouseState.item_on_cursor:
        _clear_all_highlights()
        return

    var inv_st := _determine_inv_state_under_cursor(event.global_position, MouseState.item_on_cursor)

    if inv_st['items_under'].size() < 1:
        _clear_all_highlights(true)
        return

    _clear_all_highlights(true)

    if inv_st['single_item']:
        if inv_st['can_swop']:
            _item_place.color = GameConstants.ITEM_WIDGET_COLOR_OK
            inv_st['single_item'].set_highlight(GameConstants.ITEM_WIDGET_COLOR_OK)
        else:
            _item_place.color = GameConstants.ITEM_WIDGET_COLOR_INVALID
            inv_st['single_item'].set_highlight(GameConstants.ITEM_WIDGET_COLOR_INVALID)
        return

    for item in inv_st['items_under']:
        _item_place.color = GameConstants.ITEM_WIDGET_COLOR_INVALID
        item.set_highlight(GameConstants.ITEM_WIDGET_COLOR_INVALID)



func _handle_input_mouse_click(event: InputEventMouseButton):
    if event.button_index == BUTTON_LEFT:
        if not event.pressed:
            if MouseState.item_on_cursor:

                if MouseState.item_under_cursor == MouseState.item_on_cursor:
                    MouseState.item_on_cursor.drop_item_temporary()
                    return

                var inv_st := _determine_inv_state_under_cursor(event.global_position, MouseState.item_on_cursor)

                if inv_st['items_under'].size() == 0:
                    MouseState.item_on_cursor.parent_remove_from()
                    item_widget_add(MouseState.item_on_cursor, inv_st['item_pos'])
                    MouseState.item_on_cursor = null
                elif inv_st['single_item'] and inv_st['can_swop']:
                    var tmp_widg = MouseState.item_on_cursor
                    MouseState.item_on_cursor = null
                    tmp_widg.parent_remove_from()
                    inv_st['single_item'].pickup_item_permenant()
                    item_widget_add(tmp_widg, inv_st['item_pos'])
                    MouseState.item_under_cursor = tmp_widg
                    tmp_widg.set_highlight(GameConstants.ITEM_WIDGET_COLOR_HIGHLIGHT)
                else:
                    pass
            else:
                if MouseState.item_under_cursor:
                    MouseState.item_under_cursor.pickup_item_temporary()

                return

    elif event.button_index == BUTTON_RIGHT:
        if not event.pressed:
            if MouseState.item_on_cursor:
                MouseState.item_on_cursor.drop_item_temporary()
                _clear_all_highlights()


func _clear_all_highlights(show_item_place: bool = false) -> void:
    _item_place.visible = show_item_place

    for item in _content.keys():
        if item == MouseState.item_on_cursor or item == MouseState.item_under_cursor:
            continue

        item.clear_highlight()


func _resize_grid() -> void:
    var game_inv_size := Global.ITEM_1X1_SIZE
    var inv_size = Vector2()

    inv_size.x = inventory_size.x * game_inv_size.x
    inv_size.y = inventory_size.y * game_inv_size.y

    if rect_size != inv_size:
        rect_size = inv_size


func _on_container_resized():
    _resize_grid()


func _on_container_mouse_entered():
    MouseState.inv_under_cursor = self


func _on_container_mouse_exited():
    MouseState.inv_under_cursor = null
    _clear_all_highlights()


func _on_container_gui_input(event):
    if not event.is_echo():
        _handle_input(event)

