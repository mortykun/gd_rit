extends Control

var entity: GameEntity setget _entity_set

onready var item_show := $ItemShowWidget


func _ready():
    item_show.hide()

    for slot_node in $slots.get_children():
        slot_node.set_item_info_widgets(item_show)

func show():
    for slot_node in $slots.get_children():
        slot_node.item_widget_clear()

    if entity:
        for slot_node in $slots.get_children():
            if slot_node.item_slot in entity.items:
                slot_node.item_widget_add(entity.items[slot_node.item_slot], true)

    .show()

func _entity_set(value: GameEntity) -> void:
    entity = value
