extends UIBase

onready var worn := $panel/ItemsWorn


func _ready():
    pass


func init_ui(_params = null):
    if not worn:
        return

    worn.entity = XGame.player
