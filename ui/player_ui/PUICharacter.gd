extends UIBase

onready var pl_name := $Items/Panel/lbl_name
onready var pl_level := $Items/Panel/lbl_level
#onready var pl_slist := $Items/Panel/PanelContainerStats/MarginContainer/ScrollContainerStats/ItemListStats
onready var pl_stats := $Items/Panel/PanelContainerStats/MarginContainer/ScrollContainer/StatGridContainer


func _ready():
    var lblcol_1: Label = pl_stats.get_node("lbl_header_col1")
    var lblcol_2: Label = pl_stats.get_node("lbl_header_col2")

    lblcol_1.rect_min_size.x = pl_stats.get_parent().rect_size.x * 60 * 0.01
    lblcol_2.rect_min_size.x = pl_stats.get_parent().rect_size.x * 38 * 0.01

    for gs in FactoryStat.get_stat_group(FactoryStat.Groups.ABILITIES):
        _add_stat_row(gs)

    _add_header("Current")

    for gs in FactoryStat.get_stat_group(FactoryStat.Groups.TIER_TRAC):
        _add_stat_row(gs)

    _add_header("Stats")

    for gs in FactoryStat.get_stat_group(FactoryStat.Groups.STATS):
        _add_stat_row(gs)


func init_ui(_params = null):
    if not pl_name:
        return

    _show_player_info()


func _show_player_info() -> void:
    var player := XGame.player

    if not player:
        return

    pl_name.text = player.name
    pl_level.text = "Level: %d" % player.get_level()

    for gs in FactoryStat.get_stat_group(FactoryStat.Groups.ABILITIES):
        _set_stat_row(player, gs)

    for gs in FactoryStat.get_stat_group(FactoryStat.Groups.TIER_TRAC):
        _set_stat_row(player, gs)

    for gs in FactoryStat.get_stat_group(FactoryStat.Groups.STATS):
        _set_stat_row(player, gs)


func _add_stat_row(stat: GameStat) -> void:
    var col1 := Label.new()
    var col2 := Label.new()

    pl_stats.add_child(col1)
    pl_stats.add_child(col2)

    col1.name = "%s_lbl" % stat.id
    col1.text = stat.long_name
    col2.name = "%s_val" % stat.id
    col2.align = Label.ALIGN_RIGHT


func _set_stat_row(player: GamePlayer, gs: GameStat) -> void:
    var val_col: Label = pl_stats.get_node("%s_val" % gs.id)

    if not val_col:
        return

    val_col.text = "%d" % player.get_stat_value(gs.id)


func _add_header(header: String) -> void:
    var col1 := pl_stats.get_node("lbl_header_col1").duplicate()
    var col2 := pl_stats.get_node("lbl_header_col2").duplicate()

    col1.text = header

    pl_stats.add_child(col1)
    pl_stats.add_child(col2)


func _on_btn_close_pressed():
    toggle_ui("Character", false)
