extends Control

onready var _opts = $options

func _ready():
#    _opts.add_ite


    _opts.get_popup().add_icon_item(_load_icon("character_001_icon"), "Character", 66)
    _opts.get_popup().add_icon_item(_load_icon("inventory_001_icon"), "Inventory", 77)
    _opts.get_popup().add_icon_item(_load_icon("skill_001_icon"), "Skill Tree", 88)

    _opts.get_popup().connect("id_pressed", self, "_on_opt_id_pressed")




func _load_icon(icon_name) -> ImageTexture:
    var inv_icon = ImageTexture.new()
    var inv_image = Image.new()
    inv_image.load("res://asset/sprite/misc/%s.png" % icon_name)
    inv_icon.create_from_image(inv_image)

    return inv_icon


func _toggle_ui(ui_name: String, visible: bool) -> void:
    var area: UIArea = get_parent().find_node("UIArea")

    if not area:
        Global.show_error("UI Area [%s] not found.")
        return

    area.toggle_ui(ui_name, visible)


func _on_opt_id_pressed(opt_id: int):
    if opt_id == 66:
        _toggle_ui("Character", true)
