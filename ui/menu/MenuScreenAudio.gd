extends UIBase

onready var vol_master = $Items/hbox/opts/volume_master
onready var vol_music = $Items/hbox/opts/volume_music
onready var vol_sfx = $Items/hbox/opts/volume_sfx
onready var vol_voice = $Items/hbox/opts/volume_voice
onready var vol_mute = $Items/hbox/opts/volume_mute

var _had_change := false

func _ready():
    vol_master.initialize('Master Volume', 0, 100, Settings.get_setting(vol_master.widget_tag))
    vol_music.initialize('Music Volume', 0, 100, Settings.get_setting(vol_music.widget_tag))
    vol_sfx.initialize('SFX Volume', 0, 100, Settings.get_setting(vol_sfx.widget_tag))
    vol_voice.initialize('Voice Volume', 0, 100, Settings.get_setting(vol_voice.widget_tag))
    vol_mute.initialize('Mute', Settings.get_setting(vol_mute.widget_tag))

    vol_master.connect("value_changed", self, "_on_slider_changed")
    vol_music.connect("value_changed", self, "_on_slider_changed")
    vol_sfx.connect("value_changed", self, "_on_slider_changed")
    vol_voice.connect("value_changed", self, "_on_slider_changed")
    vol_mute.connect("value_changed", self, "_on_toggle_changed")


func init_ui(_params = null):
    _had_change = false


func _on_btn_back_pressed():
    if _had_change:
        Settings.save_settings()

    change_ui("Settings")


func _on_slider_changed(ctrl: SliderWidget, value: int):
    _had_change = true
    GlobalSfx.sfx_opt_change()
    Settings.apply_setting(ctrl.widget_tag, value)


func _on_toggle_changed(ctrl: ToggleWidget, value: bool):
    _had_change = true
    Settings.apply_setting(ctrl.widget_tag, value)
