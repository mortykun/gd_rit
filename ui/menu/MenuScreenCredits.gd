extends UIBase

onready var cred_txt := $Items/hbox_credits/credits_richlbl

func _ready():
    cred_txt.push_align(RichTextLabel.ALIGN_CENTER)


    _add_header('MUSIC')
    _add_src_artist("Jazz Comedy", "from Bensound.com")
    _add_src_artist("Liturgy Of The Street by Shane Ivers", "https://www.silvermansound.com")

    cred_txt.newline()
    cred_txt.newline()
    _add_header('SFX')
    _add_src_artist("Kenny", "www.kenney.nl")


func _add_header(txt: String) -> void:
    cred_txt.push_bold()
    cred_txt.push_color(Color.yellow)
    cred_txt.add_text("-- ")
    cred_txt.add_text(txt)
    cred_txt.add_text(" --")
    cred_txt.pop()
    cred_txt.pop()
    cred_txt.newline()


func _add_src_artist(src: String, art: String) -> void:
    cred_txt.newline()
    cred_txt.add_text(src)
    cred_txt.add_text(' - ')
    cred_txt.push_bold()
    cred_txt.add_text(art)
    cred_txt.pop()


func _on_btn_back_pressed():
    change_ui("MainMenu")


