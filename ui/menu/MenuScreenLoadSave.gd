extends UIBase

onready var btn_load_create := $Items/hopts/btn_load
onready var btn_delete := $Items/hopts/btn_delete
onready var slot_items := $Items/hbox/slots

var SaveSlotScene = preload("res://ui/widget/SaveSlotWidget.tscn")

var _mode_create: bool
var _slot_headers = {}
var _active_slot = 0


func _ready():
    _init_slot_widges()


func init_ui(_params = null):
    if not slot_items:
        return

    _mode_create = _params == "create"

    if _mode_create:
        $Items/lbl_title.text = "NEW GAME"
        $Items/hopts/btn_load.text = "CREATE GAME"
    else:
        $Items/lbl_title.text = "LOAD GAME"
        $Items/hopts/btn_load.text = "LOAD GAME"

    _active_slot = 0
    _show_slots()
    _toggle_buttons()


func _show_slots():
    _slot_headers = SaveGame.get_game_headers()

    var ssw: SaveSlotWidget

    for idx in range(SaveGame.MAX_SAVES):
        ssw = _get_slot_widget(idx + 1)
        ssw.initialize({})

    for slot in _slot_headers:
        ssw = _get_slot_widget(slot)
        ssw.initialize(_slot_headers[slot])


func _get_slot_widget(slot) -> SaveSlotWidget:
    var node_name = 'slot%d' % slot

    return slot_items.get_node(node_name) as SaveSlotWidget


func _init_slot_widges():
    var slot: int
    var swidget: SaveSlotWidget
    var slot_name: String

    for idx in range(SaveGame.MAX_SAVES):
        slot = idx + 1
        slot_name = "slot%d" % slot
        swidget = SaveSlotScene.instance()
        swidget.name = slot_name
        swidget.slot_id = slot
        swidget.connect("slot_selected", self, "_slot_selected")
        slot_items.add_child(swidget)


func _slot_selected(slot: int):
    _active_slot = slot
    _toggle_buttons()


func _toggle_buttons():
    btn_load_create.disabled = true
    btn_delete.disabled = true

    var has_save = _slot_headers.get(_active_slot) != null

    if _mode_create:
        btn_load_create.disabled = has_save or _active_slot == 0
        btn_delete.disabled = not has_save
    else:
        btn_load_create.disabled = not has_save
        btn_delete.disabled = not has_save


func _on_btn_back_pressed():
    _slot_headers.clear()
    change_ui("MainMenu")


func _on_btn_load_pressed():
    _slot_headers.clear()

    if _mode_create:
        change_ui("NewGame", str(_active_slot))
    else:
        if XGame.load_game(_active_slot) != OK:
            return

        XGame.continue_game()


func _on_btn_delete_pressed():
    var ssw := _get_slot_widget(_active_slot)

    if not ssw:
        return

    SaveGame.delete_game(_active_slot)
    _slot_headers = SaveGame.get_game_headers()
    ssw.initialize({})
