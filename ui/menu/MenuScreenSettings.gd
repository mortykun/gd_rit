extends UIBase

func _ready():
    pass


func _on_btn_back_pressed():
    change_ui("MainMenu")


func _on_btn_audio_pressed():
    change_ui("Audio")


func _on_btn_vido_pressed():
    change_ui("Video")


func _on_btn_game_pressed():
    change_ui("Game")
