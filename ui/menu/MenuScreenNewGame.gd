extends UIBase

onready var pname := $Items/hbox/opts/player_name
onready var gender := $Items/hbox/opts/player_gender
onready var btn_cont := $Items/hbox/opts/btn_continue

var _slot_id: int


func init_ui(_params = null):
    if not pname:
        return

    _slot_id = int(_params)

    btn_cont.disabled = true

    pname.initialize("Your Name", "")
    gender.initialize("Your Gender", ["Male", "Female"])

    pname.connect("value_changed", self, "_on_textinput_changed")


func _on_btn_cancel_pressed():
    change_ui("LoadSave", "create")


func _on_textinput_changed(ctrl: TextInputWidget, value: String):
    if ctrl == pname:
        btn_cont.disabled = value.length() == 0


func _on_btn_continue_pressed():
    if OK == XGame.new_game(_slot_id, pname.get_text(), gender.get_option_text()):
        XGame.continue_game()
