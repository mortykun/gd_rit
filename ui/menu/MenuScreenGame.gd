extends UIBase

onready var hints = $Items/hbox/opts/hints
onready var tutorials = $Items/hbox/opts/tutorials

var _had_change := false

func _ready():
    hints.initialize('Hints', Settings.get_setting(hints.widget_tag))
    tutorials.initialize('Tutorials', Settings.get_setting(tutorials.widget_tag))

    hints.connect("value_changed", self, "_on_toggle_changed")
    tutorials.connect("value_changed", self, "_on_toggle_changed")


func init_ui(_params = null):
    _had_change = false


func _on_btn_back_pressed():
    if _had_change:
        Settings.save_settings()

    change_ui("Settings")


func _on_toggle_changed(ctrl: ToggleWidget, value: bool):
    _had_change = true
    Settings.apply_setting(ctrl.widget_tag, value)
