extends UIBase

const RESOLUTIONS = [
    Vector2(1280, 1024),
    Vector2(1600, 900),
    Vector2(1680, 1050),
    Vector2(1920, 1080),  # default
    Vector2(1920, 1200),
    Vector2(2048, 1152),
    Vector2(2560, 1080),
    Vector2(2560, 1440),
]

onready var fscreen = $Items/hbox/opts/full_screen
onready var borderless = $Items/hbox/opts/borderless
onready var vsync = $Items/hbox/opts/vsync
onready var resolution = $Items/hbox/opts/resolution

var _had_change := false


func _ready():
    fscreen.initialize('Full Screen', Settings.get_setting(fscreen.widget_tag))
    borderless.initialize('Borderless', Settings.get_setting(borderless.widget_tag))
    vsync.initialize('V-Sync', Settings.get_setting(vsync.widget_tag))
    resolution.initialize("Resolution", _valid_resolutions(), _curr_res())

    fscreen.connect("value_changed", self, "_on_toggle_changed")
    borderless.connect("value_changed", self, "_on_toggle_changed")
    vsync.connect("value_changed", self, "_on_toggle_changed")
    resolution.connect("value_changed", self, "_on_res_changed")



func init_ui(_params = null):
    _had_change = false


func _valid_resolutions() -> PoolStringArray:
    var res = PoolStringArray()

    for res_val in RESOLUTIONS:
        res.append('%dx%d' % [res_val.x, res_val.y])

    return res


func _curr_res() -> int:
    var cres = Settings.get_setting("video/res")
    var idx = 0

    for res_val in RESOLUTIONS:
        if cres == res_val:
            break

        idx += 1

    if idx >= RESOLUTIONS.size():
        idx = 3  # default

    return idx


func _on_btn_back_pressed():
    if _had_change:
        Settings.save_settings()

    change_ui("Settings")


func _on_toggle_changed(_ctrl: ToggleWidget, value: bool) -> void:
    _had_change = true
    Settings.apply_setting(_ctrl.widget_tag, value)


func _on_res_changed(_ctrl: DropDownWidget, value: int) -> void:
    _had_change = true
    Settings.apply_setting("video/res", RESOLUTIONS[value])
