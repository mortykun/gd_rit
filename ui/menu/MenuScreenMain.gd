extends UIBase


func init_ui(_params = null):
    $Items/hbox/opts/btn_continue.disabled = true
    $Items/hbox/opts/btn_loadgame.disabled = true

    if SaveGame.count_save_games() > 0:
        $Items/hbox/opts/btn_loadgame.disabled = false

        var slot = Settings.get_setting("misc/last_saveslot")

        if slot > 0 and slot <= SaveGame.MAX_SAVES:
            $Items/hbox/opts/btn_continue.disabled = false


func _on_btn_settings_pressed():
    change_ui("Settings")


func _on_btn_quit_pressed():
    Global.quit_game()


func _on_btn_loadgame_pressed():
   change_ui("LoadSave", "load")


func _on_btn_newgame_pressed():
  change_ui("LoadSave", "create")


func _on_btn_continue_pressed():
    var slot: int = Settings.get_setting("misc/last_saveslot")

    if XGame.load_game(slot) != OK:
        return

    XGame.continue_game()


func _on_btn_itemspike_pressed():
    SceneChanger.change("res://ui/spike/spike_item.tscn")

func _on_btn_credits_pressed():
   change_ui("Credits")
