shader_type canvas_item;

uniform vec4 start : hint_color;
uniform vec4 stop  : hint_color;

void fragment()
{
    COLOR = mix(start, stop, UV.x);

    COLOR.a = texture(TEXTURE,UV).a*COLOR.a;
}