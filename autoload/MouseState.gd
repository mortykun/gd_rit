extends Node

var item_on_cursor: ItemWidget setget _set_on_item
var item_under_cursor: ItemWidget setget _set_under_item
var inv_under_cursor: Control setget _set_inventory_under_cursor

var _std_cursor = load("res://asset/sprite/misc/mouse_pointer.png")
var item_img: Sprite setget _set_mouse_sprite
var _screen_size: Vector2


func _ready():
    self._set_standard_cursor()
    self._screen_size = OS.get_window_size()
    self._screen_size.x = self._screen_size.x - 1.0
    self._screen_size.y = self._screen_size.y - 1.0


func _process(_delta):
    if not item_img:
        return

    if item_on_cursor:
        item_img.position = item_img.get_global_mouse_position()

        if item_img.position.x <= 1.0 or\
           item_img.position.y <= 1.0 or\
           item_img.position.x >= _screen_size.x or\
           item_img.position.y >= _screen_size.y:
            item_img.visible = false
        else:
            item_img.visible = true
    else:
        item_img.visible = false


func clear_state() -> void:
    item_img = null
    item_on_cursor = null
    item_under_cursor = null
    inv_under_cursor = null
    _set_standard_cursor()


func has_item_on_cursor() -> bool:
    return item_on_cursor != null


func _set_mouse_sprite(value: Sprite) -> void:
    item_img = value
    item_img.visible = false


func _set_on_item(value: ItemWidget) -> void:
    item_on_cursor = value

    if not item_on_cursor:
        _set_standard_cursor()
    else:
        _set_item_cursor()


func _set_under_item(value: ItemWidget) -> void:
    if value:
        item_under_cursor = value   # just for debug purposes
    else:
        item_under_cursor = null


func _set_inventory_under_cursor(value: Control) -> void:
    inv_under_cursor = value


func _set_standard_cursor() -> void:
    Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
    Input.set_custom_mouse_cursor(self._std_cursor, Input.CURSOR_ARROW)


func _set_item_cursor() -> void:
    if not item_on_cursor or not item_on_cursor.item.game_item.inv_img:
        return

    var gi: GameItem = item_on_cursor.item.game_item
    var img_size: Vector2 = item_on_cursor.item.widget_image_size()
    var img_texture: Texture = gi.inv_img

    if not item_img:
        Global.show_error("MouseState._set_item_cursor() called, but not sprite has been assigned!")
        return

    item_img.texture = img_texture
    item_img.scale = Vector2(img_size.x / img_texture.get_width(), img_size.y / img_texture.get_height())
    item_img.centered = true
    item_img.visible = true

    Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
