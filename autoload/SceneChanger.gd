extends CanvasLayer

const SHOW_PROGRESS_AFTER = 0.02  # 20 miliseconds
const LOAD_WAIT_TIME = 10  # miliseconds

onready var _anime := $Control/AnimationPlayer
onready var _prog := $Control/ProgressBar
onready var _lbl := $Control/LoadingLabel
var _next_scene : String
var _transition : String
var _res_loader = null
var _curr_scene = null
var _trans_time : float


func _ready():
    _scene_change_done()


func change(next_scene: String, transition: String = "fade"):
    _next_scene = next_scene
    _transition = transition
    _curr_scene = get_tree().get_root().get_child(get_tree().get_root().get_child_count() -1)
    $Control.visible = true
    pause_mode = Node.PAUSE_MODE_INHERIT
    _anime.play("%s_out" % _transition)
    _lbl.text = "Loading..."
    _prog.value = 0
    _trans_time =  0.0
    _lbl.visible = false
    _prog.visible = false


func main_menu():
    change("res://ui/MainMenu.tscn")


func _do_scene_change():
    _res_loader = ResourceLoader.load_interactive(_next_scene)

    if _res_loader == null:
        Global.show_error("Resource [%s] not found" % _next_scene)
        _transition_in()
        return

    _curr_scene.queue_free()
#    _lbl.visible = true
#    _prog.visible = true
    set_process(true)


func _transition_in():
    _lbl.visible = false
    _prog.visible = false
    _res_loader = null
    _anime.play("%s_in" % _transition)


func _scene_change_done():
    $Control.visible = false
    _res_loader = null
    _anime.stop()
    pause_mode = Node.PAUSE_MODE_STOP
    set_process(false)


func _process(time: float) -> void:
    _trans_time += time

    if _res_loader == null:
        return

    var t = OS.get_ticks_msec()

    while OS.get_ticks_msec() < t + LOAD_WAIT_TIME:
        var err = _res_loader.poll()

        if err == OK:
            _lbl.text = "[%d/%d] Loading..." % [_res_loader.get_stage(), _res_loader.get_stage_count()]
            _prog.value = round(float(_res_loader.get_stage()) / _res_loader.get_stage_count() * 100)
            continue

        elif err == ERR_FILE_EOF:

            var resource = _res_loader.get_resource()
            _curr_scene = resource.instance()
            get_node("/root").add_child(_curr_scene)
            _transition_in()

        else: # error during loading
            Global.show_error("Unexpcted error [%s] while loading resource" % err)
            _transition_in()

        break

    if _trans_time > SHOW_PROGRESS_AFTER and _res_loader != null:
        _lbl.visible = true
        _prog.visible = true

