extends Node

const CONFIG_FILE = "res://config.cfg"

var _settings   = {
    'audio/mute'   : false,
    'audio/master' : 70,
    'audio/music'  : 70,
    'audio/sfx'    : 70,
    'audio/voice'  : 70,

    'video/full_screen' : false,
    'video/vsync'       : true,
    'video/borderless'  : false,
    'video/res'         : Vector2(1920, 1080),

    'misc/last_saveslot'  : 0,
    'misc/hints'          : true,
    'misc/tutorials'      : true,
    'misc/locale'         : '',
    'misc/prefer_input'   : '',

#    'input_kb': {
#        'up'      : 'w',
#        'down'    : 's',
#        'left'    : 'a',
#        'right'   : 'd',
#        'attack'  : 'alt',
#        'jump'    : 'slash',
#        'special' : 'shift',
#        'item'    : 'enter',
#    },
#
#    'input_jp': {
#        'up'      : 'DPAD Up',
#        'down'    : 'DPAD Down',
#        'left'    : 'DPAD Left',
#        'right'   : 'DPAD Right',
#        'attack'  : 'Face Button Bottom',
#        'jump'    : 'Face Button Right',
#        'special' : 'Face Button Top',
#        'item'    : 'Face Button Left',
#    },
}


func _ready():
    var fh = File.new()

    if fh.file_exists(CONFIG_FILE):
        if load_settings() != OK:
            Global.show_error('Config file corrupt, using defaults instead')

    for key in _settings.keys():
        apply_setting(key, self._settings[key], false)

    if self._settings['misc/locale'] == '':
        var loc = TranslationServer.get_locale()

        if len(loc) > 2:
            loc = loc.substr(0, 2)

        self._settings['misc/locale'] = loc

    if not fh.file_exists(CONFIG_FILE):
        save_settings()


func save_settings():
    # Save the config settings.
    # :param cfg_path: If null, the default is set to CONFIG_FILE
    Global.save_var_file(CONFIG_FILE, _settings)


func load_settings(cfg_dict = null) -> int:
    # Load settings from a saved config file.
    # :param cfg_dict dict|null: If null, use the default _settings dictionary.
    # :return: OK for success or the error code occured while loading.
    var cfg := Global.load_var_file(CONFIG_FILE)

    if not cfg:
        Global.show_error("Could not load config [file:%s]" % CONFIG_FILE)
        return ERR_FILE_CANT_READ

    if not cfg_dict:
        cfg_dict = _settings

    for key in cfg:
        cfg_dict[key] = cfg[key]

    return OK


func set_setting(path: String, value) -> void:
    # Set a setting by path.
    # :param path: The setting path.
    # :param value: The setting value.
    if not self._settings.has(path):
        Global.show_error("Setting [%s] not found." % path)

    _settings[path] = value


func get_setting(path: String):
    # Get a setting by path.
    # :param path: The setting path
    # :return: The setting value
    if not self._settings.has(path):
        Global.show_error("Setting [%s] not found." % path)
        return null

    return self._settings[path]


func apply_setting(path: String, value, set_cfg: bool = true):
    # Applys a setting to the game in real time.
    # :param path: The setting section/key path.
    # :param value: The setting value, int/string/bool whatever is needed.
    # :param set_cfg: If true, will also set the value in the config file.
    if set_cfg:
        set_setting(path, value)

    match path:
#        "misc/locale":
#            _locale_set(value)
#
        "audio/mute":
            _audio_mute(true if value == true else false)
        "audio/master":
            _audio_volume("Master", value)
        "audio/music":
            _audio_volume("Music", value)
        "audio/sfx":
            _audio_volume("SFX", value)
        "audio/voice":
            _audio_volume("Voice", value)

        "video/vsync":
            OS.set_use_vsync(true if value == true else false)
        "video/full_screen":
            _video_full_screen(value)
        "video/borderless":
            _video_borderless(value)
        "video/res":
            _video_resolution(value)

#        "input_kb/up",     "input_kb/down",    "input_kb/left", "input_kb/right",\
#        "input_kb/attack", "input_kb/special", "input_kb/jump", "input_kb/item":
#            _apply_kb_input(path.right(9), value)
#
#        "input_jp/up",     "input_jp/down",    "input_jp/left", "input_jp/right",\
#        "input_jp/attack", "input_jp/special", "input_jp/jump", "input_jp/item":
#            _apply_jp_input(path.right(9), value)


func _audio_mute(value: bool):
    for idx in range(AudioServer.get_bus_count()):
        AudioServer.set_bus_mute(idx, value)


func _audio_volume(channel: String, value):
    var cidx = AudioServer.get_bus_index(channel)

    if cidx >= 0:
        var mul = 80 - value
        var vol = mul * 0.01 * mul

        if value > 80:
            vol = 0.0 - vol

        AudioServer.set_bus_volume_db(cidx, 0.0 - vol)


func _video_full_screen(value):
    if value:
        _video_borderless(true)
        OS.window_fullscreen = true
    else:
        OS.window_fullscreen = false
        OS.window_borderless = _settings['video/borderless']
        OS.window_size = _settings['video/res']
        _video_bounce_full_screen()
        _video_bounce_borderless()


func _video_resolution(value) -> void:
    OS.window_size = value
    _video_bounce_full_screen()


func _video_borderless(value):
    OS.window_borderless = value
    _video_bounce_borderless()


func _video_bounce_full_screen() -> void:
    if OS.window_fullscreen:
        OS.window_fullscreen = false
        OS.window_fullscreen = true


func _video_bounce_borderless() -> void:
    if OS.window_borderless:
        OS.window_borderless = false
        OS.window_borderless = true


func _apply_kb_input(action: String, value):
    for old in InputMap.get_action_list(action):
        if old is InputEventKey:
            InputMap.action_erase_event(action, old)

    if value == null or value == "":
        return

    var newEvent = InputEventKey.new()

    newEvent.scancode = OS.find_scancode_from_string(value)

    InputMap.action_add_event(action, newEvent)


func _apply_jp_input(action, value):
    for old in InputMap.get_action_list(action):
        if old is InputEventJoypadButton:
            InputMap.action_erase_event(action, old)

    if value == null or value == "":
        return

    var newEvent = InputEventJoypadButton.new()

    newEvent.button_index = Input.get_joy_button_index_from_string(value)

    InputMap.action_add_event(action, newEvent)


func _locale_set(value):
    TranslationServer.set_locale(value)
