extends Node

var player: GamePlayer = null
var npc: GameNpc = null
var opponent: GameMonster = null
var zone = null
var save_slot: int = 0
var save_header := {}

func _ready():
    pass


func is_loaded() -> bool:
    return save_header.size() > 0


func clear() -> void:
    player = null
    npc = null
    opponent = null
    zone = null
    save_slot = 0
    save_header.clear()


func new_game(slot: int, name: String, gender: String) -> int:
    clear()

    player = GamePlayer.new()
    player.name = name
    player.gender = gender.to_lower()
    player.age = 21

    var header := SaveGame.new_header(slot)

    header['caption'] = '%s (%s)' % [player.name, player.gender]

    var rc := SaveGame.create_new_game(slot, header)

    save_slot = slot
    player.save_bio()

    if rc != OK:
        return rc

    return load_game(slot)


func load_game(slot: int) -> int:
    clear()

    if not SaveGame.game_slot_ok(slot):
        Global.show_error("Game slot [%d] does not exist, and cannot be loaded!" % slot)
        return ERR_DOES_NOT_EXIST

    var header := SaveGame.load_header(slot)

    if not header:
        Global.show_error("Game header could not be opened for slot [%d]!" % slot)
        return ERR_CANT_OPEN

    var plyr := GamePlayer.new()

    save_slot = slot
    save_header = header
    save_header['slot'] = slot

    if not plyr.load_all():
        Global.show_error("Game player could not be loaded for slot [%d]!" % slot)
        return ERR_CANT_OPEN

    player = plyr

    Settings.set_setting('misc/last_saveslot', slot)
    Settings.save_settings()

    return OK


func continue_game() -> void:
#    SceneChanger.change("res://ui/spike/poker_table.tscn")
    SceneChanger.change("res://game/zone/00_unittest/zone_00_unittest.tscn")


func player_equip_item(item_slot: int, item: ItemWidget) -> ItemWidget:
    # Equips an item, returns the replaced item, or null if not item was there.
    # Note that if item is null, that effectively un-equips the item.
    if not player:
        return null

    var res: ItemWidget = player.items.get(item_slot)

    player.items[item_slot] = item

    player.rebuild_stats()

    return res
