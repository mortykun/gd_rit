extends Node

# TODO
# [ ] Encrypt the save game files

export var MAX_SAVES = 5
const SGPATH = "res://_savegame/save-%02d"
const SAVE_MASK = '%s%02d%s%s'
const SAVE_HEADER = 'header'
const SAVE_ROCK = ".rock"
const SAVE_ROLL = ".roll"


func _ready():
    pass


func count_save_games() -> int:
    var cnt = 0

    for idx in range(MAX_SAVES):
        if game_slot_ok(idx + 1):
            cnt += 1

    return cnt


func slot_open(slot: int) -> bool:
    return not game_slot_ok(slot)


func get_game_headers() -> Dictionary:
    var res = {}
    var slot: int
    var header_obj: Dictionary

    for idx in range(MAX_SAVES):
        slot = idx + 1

        if game_slot_ok(slot):
            header_obj = load_header(slot)

            if header_obj.size() > 0:
                res[slot] = header_obj

    return res


func load_header(slot: int) -> Dictionary:
    return load_file(slot, SAVE_HEADER)


func load_file(slot: int, file_id: String) -> Dictionary:
    # Loads the specific file. This will do a rolling load, and check
    #  which is the latest file and attempt to load that one before loading the other.
    # If a file is loaded it removes the other rock or roll file.
    if slot < 1 or slot > MAX_SAVES:
        Global.show_error("Cannot load save file, slot [%s] not valid!" % slot)
        return {}

    var spath := _slot_path(slot)
    var dir := Directory.new()

    if dir.open(spath) != OK:
        return {}

    var fh := File.new()
    var files := _get_rock_roll_files(spath, fh, file_id)
    var obj
    var loaded_file : String

    if not files:
        return {}

    for fname in files:
        obj = Global.load_var_file(fname)

        if obj:
            loaded_file = fname
            break

    if obj:
        for fname in files:
            if fname != loaded_file:
                dir.remove(fname)

        return obj

    return {}


func save_header(header: Dictionary) -> int:
    return save_file(header['slot'], SAVE_HEADER, header)


func save_file(slot: int, file_id: String, save_obj: Dictionary) -> int:
    # Loads the specific file. This will do a rolling save, and check
    #  which is the latest file and attempt to save to the other file.
    # :return: The return code from the save function
    if slot < 1 or slot > MAX_SAVES:
        Global.show_error("Cannot save file, slot [%s] not valid!" % slot)
        return ERR_INVALID_DATA

    var spath := _slot_path(slot)
    var dir := Directory.new()

    if dir.open(spath) != OK:
        return ERR_CANT_OPEN

    var fh := File.new()
    var files := _get_rock_roll_files(spath, fh, file_id)
    var fname

    if not files:
        fname = _slot_file_path(spath, file_id, SAVE_ROCK)
    else:
        if files.size() > 1:
            fname = files[1]
        else:
            if files[0].endswith(SAVE_ROCK):
                fname = _slot_file_path(spath, file_id, SAVE_ROCK)
            else:
                fname = _slot_file_path(spath, file_id, SAVE_ROLL)

    return Global.save_var_file(fname, save_obj)


func delete_game(slot: int) -> int:
    # Delete the save game at the specified slot.
    var spath := _slot_path(slot)

    return Global.rmdir_recursive(spath)


func create_new_game(slot: int, header: Dictionary) -> int:
    # Create a new game
    if game_slot_ok(slot):
        return ERR_ALREADY_EXISTS

    var spath := _slot_path(slot)
    var dir := Directory.new()

    if dir.make_dir_recursive(spath) != OK:
        return ERR_CANT_CREATE

    return save_file(slot, SAVE_HEADER, header)


func game_slot_ok(slot: int) -> bool:
    # Check if a game slot is okay, ready of use or not
    if slot < 1 or slot > MAX_SAVES:
        Global.show_error("Slot [%s] not valid (out of bounds)!" % slot)
        return false

    return game_slot_file_exists(slot, SAVE_HEADER)


func game_slot_file_exists(slot: int, file_id: String) -> bool:
    # Check if a file in the game slot is okay
    var spath := _slot_path(slot)
    var dir := Directory.new()

    if dir.open(spath) != OK:
        return false

    var fh = File.new()

    if fh.file_exists(_slot_file_path(spath, file_id, SAVE_ROCK)):
        return true

    if fh.file_exists(_slot_file_path(spath, file_id, SAVE_ROLL)):
        return true

    return false


func _slot_path(slot: int) -> String:
    # The directory path for the given game slot
    return SGPATH % [slot]


func _slot_file_path(spath: String, fname: String, ext_id: String) -> String:
    # Returns save file in the slot directory
    return '%s/%s%s' % [spath, fname, ext_id]


func _get_rock_roll_files(spath: String, fh: File, file_id: String) -> Array:
    # Get the two rock and roll files and put them in order of newest to oldest
    var res := []

    var rock := _slot_file_path(spath, file_id, SAVE_ROCK)
    var roll := _slot_file_path(spath, file_id, SAVE_ROCK)

    if fh.file_exists(rock):
        res.append(rock)

    if fh.file_exists(roll):
        if not res.size():
            res.append(roll)
        else:
            if fh.get_modified_time(rock) > fh.get_modified_time(roll):
                res.append(roll)
            else:
                res.insert(0, roll)

    return res


func new_header(slot: int, diff: int = 2) -> Dictionary:
    # Create a new game dictionary.
    # :param slot: The save game slot
    # :param diff: The starting diffuculty
    # :returns: The full new player save game file
    var curr_time = OS.get_unix_time()

    var sg = {
        'slot':slot,
        'caption': '<New Game>',
        'difficulty':diff,
        'deaths':0,
        'date_created': curr_time,
        'date_saved': curr_time,
        'time_played':0.0,
        'game_over':false,
        'cheated':false,
        'continues':0,
        'zone_id':0,
        'zone_name':'Charactor Creation',
        'waypoint': null,
        'in_combat': false,
    }

    return sg
