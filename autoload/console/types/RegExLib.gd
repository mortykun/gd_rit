extends Object


const _patterns = {
    1: '^(1|0|true|false)$',  # bool
    2: '^\\d+$',  # int
    3: '^[+-]?([0-9]*[\\.\\,]?[0-9]+|[0-9]+[\\.\\,]?[0-9]*)([eE][+-]?[0-9]+)?$',  # float
}

var _compiled = {}


func get_pattern(type_id: int) -> RegEx:

    if !_compiled.has(type_id):
        var r = RegEx.new()
        r.compile(_patterns[type_id])

        if r and r is RegEx:
            _compiled[type_id] = r
        else:
            return null

    return _compiled[type_id]
