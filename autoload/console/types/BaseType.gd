extends Object


var value
#var name: String setget _set_protected
var type_id: int setget _set_protected
var rematch: String setget _set_protected

func _ready():
    self.value = null
#    self.name = ''
    self.type_id = -1
    self.rematch = ''


func check(_value) -> int:
    var rematch = Console.RegExLib.get_pattern(type_id)

    if rematch and rematch is RegEx:
        rematch = rematch.search(_value)

        if rematch and rematch is RegExMatch:
            return OK

    return FAILED


func get_value():  # Variant
    return value


func _set_protected(_value):
    pass
