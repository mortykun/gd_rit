extends Object

const Types = preload('Types.gd')

#enum ARGASSIG {
#  OK,
#  FAILED,
#  CANCELED
#}
#

var name: String
var type_id: int setget _set_protected
var value setget set_value
var default


func _ready():
    self.name = ''
    self.value = null
    self.type_id = 0
    self.default = null


func _init(_name: String, _type_id: int = 0):
    self.name = _name

    if typeof(_type_id) == TYPE_OBJECT:
        self.type_id = _type_id
    else:
        self.type_id = Types.createT(_type_id)


func set_value(_value: String) -> int:
    var set_check = type.check(_value)

    if set_check == OK:
        value = type.getValue()

    return set_check


func _set_protected(_value):
    pass





# @param  Array<Argument>  args
static func to_string(args):  # string
    var result = ''

    var argsSize = args.size()
    for i in range(argsSize):
        if args[i].name:
            result += args[i].name + ':'

        result += args[i].type.name

        if i != argsSize - 1:
            result += ' '

    return result
