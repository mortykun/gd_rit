# Godot Console main script
# Copyright (c) 2016 Hugo Locurcio and contributors - MIT license
# 2020: Ammended by Nicolas Milicevic to make work for Godot 3.2.x and some tweaks

extends CanvasLayer

var RegExLib = preload('types/RegExLib.gd').new()

func _ready():
    pass



func register_command(_name: String, _args: Array) -> bool:
    # Registers a new command
    # :param name: The new command name.
    # :param args: Argument list.
    # :return: True on success or false on failure.
#    if args.has("target") and args.target != null and args.has("args"):
#        if args.target.has_method(name):
#            var argsl = []
#
#            for arg in args.args:
#                if typeof(arg) == TYPE_ARRAY:
#                    argsl.append(Argument.new(arg[0], arg[1]))
#                elif typeof(arg) == TYPE_STRING:
#                    argsl.append(Argument.new(arg))
#                elif typeof(arg) == TYPE_INT or typeof(arg) == TYPE_OBJECT:
#                    argsl.append(Argument.new(null, arg))
#                else:
#                    echo("[color=#ff8888][ERROR][/color] Failed adding command %s. Invalid arguments!" % name)
#                    return false
#
#            args.args      = argsl
#            commands[name] = args
#        else:
#            echo("[color=#ff8888][ERROR][/color] Failed adding command %s. The target has no such function!" % name)
#            return false
#
#        return true

#    echo("[color=#ff8888][ERROR][/color] Failed adding command %s. Invalid arguments!" % name)
    return false

