extends Object


func _init():
    Console.register_command("echo", {
        descr  = "Echo a string in the console",
        args   = [['message', TYPE_STRING]],
        target = Console
    })

    Console.register_command("history", {
        descr  = "Print commands used during the session",
        args   = [],
        target = Console
    })

    Console.register_command("cmdlist", {
        descr  = "Lists all available console commands",
        args   = [],
        target = Console
    })

    Console.register_command("cvarlist", {
        descr  = "Lists all available console vars",
        args   = [],
        target = Console
    })

    Console.register_command("help", {
        descr  = "Outputs usage instructions",
        args   = [],
        target = Console
    })

    Console.register_command("quit", {
        descr  = "Exits the application",
        args   = [],
        target = Console
    })

    Console.register_command("clear", {
        descr  = "Clear the console",
        args   = [],
        target = Console
    })

    Console.register_command("version", {
        descr  = "Shows game and engine vesion",
        args   = [],
        target = Console
    })
