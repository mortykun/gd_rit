extends Node

const CM_2_INCH_CONV   = 0.3937008
const INCH_2_FEET_CONV = 0.08333333
const ITEM_1X1_SIZE = Vector2(67, 67)    # this is the size of a 1x1 item for all inventory items

#var _img_cache = {}


func _ready() -> void:
    randomize()


func quit_game() -> void:
    # Exit the game, todo do any save game clenaup here
    get_tree().quit()


func show_error(msg: String) -> void:
    # Show an error message
    # :param msg: The error message to show
    print("ERROR: %s" % msg)


func dict_getset_array(targ: Dictionary, array_id: String) -> Array:
    # Helper method to set an array item in a dictionary
    # :param targ: The dictionary to set the array in
    # :param array_id: The array identifer to set
    # :return: The array object
    var iarr = targ.get(array_id)

    if iarr == null:
        iarr = Array()
        targ[array_id] = iarr

    return iarr


func dict_getset_dict(targ: Dictionary, dict_id: String) -> Dictionary:
    # Helper method to set a dictionary item in a dictionary
    # :param targ: The dictionary to set the array in
    # :param dict_id: The dictionary identifer to set
    # :return: The dictionary object
    var idic = targ.get(dict_id)

    if idic == null:
        idic = Dictionary()
        targ[dict_id] = idic

    return idic


func datetime_to_string(dt: Dictionary) -> String:
    # Returns the datetime time as string
    # :param dt: The datetime object
    # :return: The current time in 'YYYY.MM.DD HH:MM:SS' format
    return "%s-%02d-%02d %02d:%02d:%02d" % [dt['year'], dt['month'], dt['day'], dt['hour'], dt['minute'], dt['second']]


func time_2_str(in_time: float) -> String:
    # Converts the time to a string
    # :param in_time: The time as a float
    # :return: The time 'HH:MM:SS' representation
    var time = int(in_time)
    var hours = time / 3600
    var minutes = time % 3600 / 60
    var seconds = time % 60

    return "%02d:%02d:%02d" % [hours, minutes, seconds]


func load_json_file(pth: String) -> Dictionary:
    # Loads a json file as a dictionary object
    # :param pth: The full path fo the file to load
    # :return: The loaded json dictionary, an empty diction if not loaded
    var fh = File.new()
    var fhrc: int = fh.open(pth, File.READ)

    if fhrc != OK:
        show_error("Json file not found! [file:%s]" % [pth])
        return {}

    var content = fh.get_as_text()
    fh.close()

    var jres: JSONParseResult = JSON.parse(content)

    if jres.error_string:
        show_error("Error parsing json file! [file:%s, line:%d, msg:%s]" % [pth, jres.error_line, jres.error_string])
        return {}

    return jres.result


func save_json_file(pth: String, data: Dictionary) -> int:
    # Saves a dictionary to a json file
    # :param pth: The full destination path of the file to write
    # :param data: The data dictionary to write
    # :returns: OK if everything was fine or the file error code
    var fh = File.new()
    var rc: int

    rc = fh.open(pth, File.WRITE)

    if rc:
        show_error("Could not open file for write [%s]" % pth)
        return rc

    fh.store_line(to_json(data))
    fh.close()

    return OK


func load_var_file(pth: String) -> Dictionary:
    # Loads a json file as a dictionary object
    # :param pth: The full path fo the file to load
    # :return: The loaded json dictionary, an empty diction if not loaded
    var fh = File.new()
    var fhrc: int = fh.open(pth, File.READ)

    if fhrc != OK:
        show_error("File not found! [file:%s]" % [pth])
        return {}

    var content = fh.get_as_text()
    fh.close()

    var vres = str2var(content)

    if vres == null or typeof(vres) != TYPE_DICTIONARY:
        show_error("Error parsing var file! [file:%s] is not null not a dictionar " % [pth])
        return {}

    return vres


func save_var_file(pth: String, data: Dictionary) -> int:
    # Saves a variable to a var file
    # :param pth: The full destination path of the file to write
    # :param data: The data dictionary to write
    # :returns: OK if everything was fine or the file error code
    var fh = File.new()
    var rc: int

    rc = fh.open(pth, File.WRITE)

    if rc:
        show_error("Could not open file for write [%s]" % pth)
        return rc

    fh.store_line(var2str(data))
    fh.close()

    return OK


func rmdir_recursive(pth: String) -> int:
    # Recusvily remove all the contents of a directory including the directory itself
    # :param pth: The directory to remove
    # :return: OK on success or the error code that caused the failure
    var dir = Directory.new()
    var rc: int
    var file_name: String

    if dir.open(pth) != OK:
        return ERR_DOES_NOT_EXIST

    dir.list_dir_begin(true)

    while true:
        file_name = dir.get_next()

        if not file_name:
            break

        if dir.current_is_dir():
            rc = rmdir_recursive(file_name)

            if rc != OK:
                return rc

            continue

        rc = dir.remove(file_name)

        if rc != OK:
            return rc

    return dir.remove(pth)


func op_add(val1: float, val2: float) -> float:
    # Operator add function
    # :param val1: The value to be added
    # :param val2: The value to be added
    # :return: val1 + val2
    return val1 + val2


func op_div(val1: float, val2: float) -> float:
    # Operator divide function
    # :param val1: The value to be divided
    # :param val2: The divided by
    # :return: val1 / val2, unless val2 is zero, then return zero
    if val2 == 0.0:
        return 0.0

    return val1 / val2


func op_mul(val1: float, val2: float) -> float:
    # Operator multiply function
    # :param val1: The value to be multiplied
    # :param val2: The value to be multiplied
    # :return: val1 - val2
    return val1 * val2


func op_sub(val1: float, val2: float) -> float:
    # Operator subtract function
    # :param val1: The value to be subtracted
    # :param val2: The value to be subtracted
    # :return: val1 - val2
    return val1 - val2


func roll_dice(rd: String) -> float:
    # Rolls the dice string, ie 1d10+20 and returns the result
    var parts = rd.split('+', true, 2)
    var tot = 0
    var dc
    var idx: int
    var cnt: int
    var dice: int

    for pt in parts:
        dc = pt.split('d')

        if dc.size() == 0:
            continue

        if dc.size() == 1:
            tot += dc[0].to_int()
            continue

        idx = 0
        cnt = dc[0].to_int()
        dice = dc[1].to_int()

        if dice < 1:
            continue

        while idx < cnt:
            tot = 1 + (randi() % dice)
            idx += 1

    return tot


func roll_coinflip() -> bool:
    return randi() % 2 == 0


func roll_irange(from_val: int, to_val: int) -> int:
    # Roll an integer between a from and to value
    # :param from_val: The from value
    # :param to_val: The to value
    # :return: The rolled value
    var rng = to_val - from_val + 1

    return (randi() % rng) + from_val


func roll_frange(from_val: float, to_val: float) -> float:
    # Roll a float between a from and to value
    # :param from_val: The from value
    # :param to_val: The to value
    # :return: The rolled value
    return rand_range(from_val, to_val)


func roll_vrange(ft_val: Vector2) -> float:
    # Roll a number between a from and to value
    # :param ft_val: The from and to value in a vector2
    # :return: The rolled value
    return roll_range(ft_val.x, ft_val.y)


func roll_range(from_val: float, to_val: float) -> float:
    # Roll a number between a from and to value - uses floats but is integer based
    # :param from_val: The from value
    # :param to_val: The to value
    # :return: The rolled value
    var rng = int((round(to_val) - round(from_val)) + 1)

    return (randi() % rng) + floor(from_val)


func roll_str_range(ft: String, sep: String = ':') -> float:
    # Roll a number between a from and to string
    # :param ft: The from to string seperated by a colon
    # :param sep: The separator to use
    # :return: The rolled value
    var parts = ft.split(sep, true, 2)

    if len(parts) != 2:
        return -1.0

    return roll_range(parts[0].to_float(), parts[1])


func roll_weight_start() -> Dictionary:
    # Start rolling a weighted object drop.
    # :return: The weigh rolling object
    var wo = {'tot': 0.0, 'sel': [] }

    return wo


func roll_weight_append(wo: Dictionary, obj, weight: float) -> void:
    # Add an item to weighted rolling
    # :param wo: The weigh object returned from 'roll_weight_start()'
    # :param obj: The object to be returned if it is rolled
    # :param weight: The weight of the object
    wo.tot += weight
    wo.sel.append([obj, wo.tot])


func roll_weight_conclude(wo: Dictionary):
    # Rolls and returns the rolled object based on the weight
    # :param wo: The weigh object returned from 'roll_weight_start()'
    # :return: The weighted object, or null if the wo param is empty
    if wo.size() == 0 or wo.tot <= 0.0 or wo.sel.size() == 0:
        return null

    var sidx = rand_range(0.0, wo.tot)

    for sitem in wo.sel:
        if sidx <= sitem[1]:
            wo.clear()
            return sitem[0]

    return null


func roll_weighted(objects: Dictionary):
    # Roll objects with their respective weights
    # :param objects: The objects to roll for
    # :return: The randomly selected object
    var wo = roll_weight_start()
    var weight: float

    for obj in objects:
        weight = objects[obj]

        if weight > 0.0:
            roll_weight_append(wo, obj, weight)

    return roll_weight_conclude(wo)


func convert_cm_2_inches(cm: float) -> float:
    # Converts centimeters to inches
    return cm * CM_2_INCH_CONV


func convert_inches_as_feet(inch: float, long: bool = false) -> String:
    # Converts inches to a feet equivalent string
    var feet = stepify(inch * INCH_2_FEET_CONV, 0.1)
    var parts = str(feet).split('.')
    var res = ''

    if inch < 0.01:
        if long:
            return '0 feet'

        return "0'"

    if parts[0] == '0':
        pass
    elif parts[0] == '1':
        if long:
            res += '%s foot' % parts[1]
        else:
            res += "%s'" % parts[1]
    else:
        if long:
            res += '%s feet' % parts[0]
        else:
            res += "%s'" % parts[0]

    if parts.size() > 1:
        if res:
            res += ' '

        if long:
            res += '%s inches' % parts[1]
        else:
            res += '%s"' % parts[1]

    return res


func float_2_str_min_decimals(value: float, max_decimals: int, min_decimals: int = 0) -> String:
    if max_decimals < 1:
        return '%.0f' % value

    var tmp = '%.*f' % [max_decimals, value];
    var splt = tmp.rstrip('0').split('.')
    var dec: String = splt[1]

    while dec.length() < min_decimals:
        dec += '0'

    if dec.length() < 1:
        return splt[0]

    return '%s.%s' % [splt[0], dec]


func load_recursive_resources(dest               : Dictionary,
                              dir_path           : String,
                              ext_list           : Array,
                              skip_initial_files : bool = true) -> int:
    # Loads all the objects recursively in the directory
    # :param dest: The destination dictionary to stores the loaded objects
    # :param dir_path: The path to start loading from
    # :param ext_list: Optionally pass in a list of extension types to load, else it loads all
    # :param skip_initial_files: Will skip files in the initial directory if true
    # :return: Success or error code
    var dir = Directory.new()
    var file_name: String

    assert(dir.dir_exists(dir_path))

    if not dir.open(dir_path) == OK:
        Global.show_error("Could not read stat directory: %s" % dir_path)
        return ERR_FILE_NOT_FOUND

    dir.list_dir_begin(true)

    while true:
        file_name = dir.get_next()

        if not file_name:
            break

        if dir.current_is_dir():
            var rc := load_recursive_resources(
                dest,
                dir_path.plus_file(file_name),
                ext_list,
                false)

            if rc != OK:
                return rc

            continue

        if skip_initial_files:
            continue

        if ext_list:
            if not ext_list.has(file_name.get_extension()):
                continue

        var res_obj: Object = load(dir_path.plus_file(file_name))
        # var res_id: String = file_name.substr(0, file_name.length() - (file_name.get_extension().length() + 1))
        var res_id = res_obj.id

        if file_name.get_extension() == 'tres':
            dest[res_id] = res_obj
        elif file_name.get_extension() == 'gd':
            dest[res_id] = res_obj.new()
        else:
            show_error("Don't know what do with file extension[%s]" % file_name.get_extension())

    return OK
